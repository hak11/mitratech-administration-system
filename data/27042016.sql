-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Inang: localhost
-- Waktu pembuatan: 27 Apr 2016 pada 17.51
-- Versi Server: 5.5.47-0ubuntu0.14.04.1
-- Versi PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `mitratech_temp1`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Struktur dari tabel `instansi`
--

CREATE TABLE IF NOT EXISTS `instansi` (
  `id_instansi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_instansi` varchar(100) NOT NULL,
  `logo` varchar(255) NOT NULL DEFAULT 'noimage.jpg',
  PRIMARY KEY (`id_instansi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `instansi`
--

INSERT INTO `instansi` (`id_instansi`, `nama_instansi`, `logo`) VALUES
(1, 'KEMENTERIAN PEMUDA DAN OLAHRAGA REPUBLIK INDONESIA', '73ad8c60a6cd6e1cd20cb0dd59492608logo_kemenpora.jpg'),
(2, 'KEMENTERIAN PERHUBUNGAN REPUBLIK INDONESIA', '8ca313e076190c5556d21a31e3904774logo_kementrian_perhubungan.png'),
(3, 'KEMENTERIAN KESEHATAN REPUBLIK INDONESIA', '56fdab7dd5248b072bbe2265536302adkemenkes.png'),
(4, 'KEMENTERIAN DALAM NEGRI REPUBLIK INDONESIA', '1d4e926b7ff345fa31915848ce1ac125logo_kemendagri.png'),
(5, 'KEMENTERIAN KOMUNIKASI DAN INFORMASI REPUBLIK INDONESIA', 'c41a301d4344474e813bbcead2717b22logo_kemkominfo.jpg'),
(6, 'KEMENTERIAN KEUANGAN REPUBLIK INDONESIA', '7140a28b1b131e85903db576b06d8d53logo_kemenkeu.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lampiran_project`
--

CREATE TABLE IF NOT EXISTS `lampiran_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `nama` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `lampiran_project`
--

INSERT INTO `lampiran_project` (`id`, `token`, `nama`, `tanggal`) VALUES
(7, '19782f8b076f39fe61692db36c212174', '19782f8b076f39fe61692db36c212174_-_zxc.png', '2016-04-27 05:53:26'),
(8, '19782f8b076f39fe61692db36c212174', '19782f8b076f39fe61692db36c212174_-_asd.png', '2016-04-27 05:53:26'),
(9, '191ec02a4b749d50bf648d1a20e46755', '191ec02a4b749d50bf648d1a20e46755_-_Screenshot from 2016-04-11 18:04:00.png', '2016-04-27 07:23:48'),
(10, '191ec02a4b749d50bf648d1a20e46755', '191ec02a4b749d50bf648d1a20e46755_-_Screenshot from 2016-04-08 13:04:23.png', '2016-04-27 07:23:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `partner`
--

CREATE TABLE IF NOT EXISTS `partner` (
  `id_partner` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `nama_pic` varchar(50) NOT NULL,
  `no_tlf` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  PRIMARY KEY (`id_partner`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data untuk tabel `partner`
--

INSERT INTO `partner` (`id_partner`, `nama`, `nama_pic`, `no_tlf`, `email`, `alamat`) VALUES
(66, 'Suwardiman', 'Suwardiman', '', '', ''),
(67, 'Riki Susanto', 'Riki Susanto', '', '', ''),
(68, 'Muhammad A.Amrulloh', 'Muhammad A.Amrulloh', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id_project` int(11) NOT NULL AUTO_INCREMENT,
  `perusahaan` varchar(50) NOT NULL,
  `status_rekan` tinyint(1) NOT NULL,
  `id_rekan` int(11) NOT NULL,
  `nama_pekerjaan` varchar(255) NOT NULL,
  `bidang_pekerjaan` varchar(100) NOT NULL,
  `lokasi_pekerjaan` varchar(50) NOT NULL,
  `no_pekerjaan` varchar(100) NOT NULL,
  `tanggal_pekerjaan` date NOT NULL,
  `nilai_kontrak` float NOT NULL,
  `mata_uang` varchar(10) NOT NULL,
  `tanggal_rencana_mulai` date NOT NULL,
  `tanggal_rencana_akhir` date NOT NULL,
  `link` varchar(255) NOT NULL,
  `file_lampiran` text NOT NULL,
  `status_project` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_project`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `project`
--

INSERT INTO `project` (`id_project`, `perusahaan`, `status_rekan`, `id_rekan`, `nama_pekerjaan`, `bidang_pekerjaan`, `lokasi_pekerjaan`, `no_pekerjaan`, `tanggal_pekerjaan`, `nilai_kontrak`, `mata_uang`, `tanggal_rencana_mulai`, `tanggal_rencana_akhir`, `link`, `file_lampiran`, `status_project`) VALUES
(3, 'PT.MITRATECH ANDAL SINERGIA', 0, 7, 'PENYEMPURNAAN APLIKASI SISTEM MONITORING ASSET', 'TELEMATIKA,PEMETAAN', 'DKI JAKARTA', 'PL.101/57/20/DJPL-15', '2016-03-23', 145225000, 'rupiah', '2016-03-23', '2016-03-23', '', '', 0),
(4, 'PT.MITRATECH ANDAL SINERGIA', 0, 7, 'PENGEMBANGAN REVITALISASI APLIKASI SISTEM INFORMASI EKSEKUTIF KEPEGAWAIAN', 'PEMETAAN', 'DKI JAKARTA', 'PL.101/57/20/DJPL-15', '2015-08-12', 523765000, 'rupiah', '2015-08-11', '2015-11-08', '', '', 1),
(6, 'PT.MITRATECH ANDAL SINERGIA', 0, 5, 'asdsd', 'TELEMATIKA,PEMETAAN', 'asd', 'asdsd', '2016-04-26', 51561600, 'rupiah', '2016-04-26', '2016-04-26', '', '', 0),
(7, 'PT.MITRATECH ANDAL SINERGIA', 0, 6, 'asd', 'TELEMATIKA', 'asd', 'asd', '2016-04-27', 12312300, 'rupiah', '2016-04-27', '2016-04-27', '', '19782f8b076f39fe61692db36c212174', 1),
(8, 'PT.MULIA ABADI SUPLAI', 0, 7, 'asd', 'TELEMATIKA,PEMETAAN,LAINNYA', 'asdsad', 'asdasd', '2016-04-27', 123213000, 'rupiah', '2016-04-27', '2016-04-27', '', '191ec02a4b749d50bf648d1a20e46755', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekanan`
--

CREATE TABLE IF NOT EXISTS `rekanan` (
  `id_rekanan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_instansi` int(11) NOT NULL,
  `nama_direktorat` varchar(100) NOT NULL,
  `nama_pic` varchar(50) DEFAULT NULL,
  `no_tlf_pic` varchar(20) DEFAULT NULL,
  `alamat` text,
  PRIMARY KEY (`id_rekanan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `rekanan`
--

INSERT INTO `rekanan` (`id_rekanan`, `nama_instansi`, `nama_direktorat`, `nama_pic`, `no_tlf_pic`, `alamat`) VALUES
(1, 4, 'SEKRETARIAT JENDRAL', '', '', ''),
(2, 2, 'SEKRETARIAT JENDRAL', '', '', ''),
(3, 1, 'SEKRETARIAT JENDRAL', '', '', ''),
(4, 3, 'SEKRETARIAT JENDRAL', '', '', ''),
(5, 5, 'SEKRETARIAT JENDRAL', '', '', ''),
(6, 3, 'DIREKTORAT  PUSAT DATA DAN INFORMASI', '', '', ''),
(7, 2, 'DIREKTORAT JENDRAL PERHUBUNGAN LAUT', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekanan_swasta`
--

CREATE TABLE IF NOT EXISTS `rekanan_swasta` (
  `id_rekanan_swasta` int(11) NOT NULL AUTO_INCREMENT,
  `nama_rekanan` varchar(50) NOT NULL,
  `no_tlf` varchar(20) DEFAULT NULL,
  `nama_pic` varchar(50) DEFAULT NULL,
  `no_tlf_pic` varchar(20) DEFAULT NULL,
  `logo` varchar(255) NOT NULL DEFAULT 'noimage.jpg',
  `alamat` text,
  PRIMARY KEY (`id_rekanan_swasta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `rekanan_swasta`
--

INSERT INTO `rekanan_swasta` (`id_rekanan_swasta`, `nama_rekanan`, `no_tlf`, `nama_pic`, `no_tlf_pic`, `logo`, `alamat`) VALUES
(1, 'PT.ANUGRAH SUMBER MAKMUR (PT.MINAMAS)', '', '', '', 'noimage.jpg', 'JAKARTA'),
(2, 'PT.BUMITAMA GUNAJAYA ABADI', '', '', '', 'noimage.jpg', 'JAKARTA\r\n'),
(3, 'PT.WAINDO SPEC TERRA', '', '', '', 'noimage.jpg', 'JAKARTA'),
(4, 'PT.BERCA HARDAYA PERKASA', '', '', '', 'noimage.jpg', 'JAKARTA');

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_lampiran`
--

CREATE TABLE IF NOT EXISTS `temp_lampiran` (
  `token` varchar(30) NOT NULL,
  `nama` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tenaga_ahli`
--

CREATE TABLE IF NOT EXISTS `tenaga_ahli` (
  `id_tenaga_ahli` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `pendidikan_terakhir` varchar(20) NOT NULL,
  `keahlian` text NOT NULL,
  `kondisi` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_tenaga_ahli`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81 ;

--
-- Dumping data untuk tabel `tenaga_ahli`
--

INSERT INTO `tenaga_ahli` (`id_tenaga_ahli`, `nama`, `tempat_lahir`, `tanggal_lahir`, `pendidikan_terakhir`, `keahlian`, `kondisi`) VALUES
(1, 'Abdul Aziz, A.Md. ', 'Bekasi', '1989-10-14', 'D3', 'Administrasi Operator ', 1),
(2, 'Abdul Halim ', 'Lhokseumawe', '1970-12-23', 'S1', 'Ahli Programmer', 1),
(3, 'Adhi Utama Nur Zakia,S.Kom', 'Kediri', '1985-05-21', 'S1', 'Ahli Administration Database', 1),
(4, 'Aditya Riyadi Soeroso,S.T., M.E', 'Yogyakarta', '1960-03-29', 'S2', 'Ahli Administration Database', 1),
(5, 'Aditya Tanjung,S.Kom', 'Surabaya', '1987-01-22', 'S1', 'Ahli Administration Database', 1),
(6, 'Agus Gustiana, S.Kom.', 'Kawali-Ciamis', '1975-08-14', 'S1', 'Ahli Web Design', 1),
(7, 'Agus Iskandar', 'Jakarta', '1975-08-10', 'S2', 'Ahli Programmer', 1),
(8, 'Albaar Rubhasy, S.Si., MTI.', 'Jakarta', '1981-10-17', 'S2', 'Ahli Security', 1),
(9, 'Anita Ratnasari,S.Kom,.M.Kom', 'Jakarta', '1981-03-18', 'S2', 'Ahli Programmer', 1),
(10, 'Anita Wulansari, S.Kom.', 'Surabaya', '1987-10-15', 'S1', 'Ahli Programmer', 1),
(11, 'Antonius Sukanto', 'Pontianak', '1985-02-09', 'S1', 'AhliDatabase Administrator', 1),
(12, 'Aries Indrabayu Yunandar', '', '0000-00-00', 'S1', 'Ahli Programmer', 1),
(13, 'Arif Rahman Hakim, ST', 'Jombang', '1991-09-30', 'S1', 'Administrator Perkantoran', 1),
(14, 'Aris Habiburrahman, S.Si.', 'Yogyakarta', '1970-01-25', 'S1', 'Ahli Programmer', 1),
(16, 'Awan Cahyadi,S.Kom', 'Jayapura', '1981-02-13', 'S1', 'Ahli Programmer', 1),
(18, 'Bagus Sasmito Hadi,S.T', 'Malang', '0000-00-00', 'S1', 'Ahli Administration Database', 1),
(19, 'Bayu Waseso,S.T.,M.Kom', 'Jakarta', '1973-05-14', 'S2', 'Ahli Sistem Analis,Team Leader', 1),
(20, 'Budi Setiawan', 'Jakarta', '1970-03-03', 'S1', 'Ahli Programmer', 1),
(21, 'Dani Irawan,S.T', 'Jakarta', '1971-11-02', 'S1', 'Ahli Sistem Analis', 1),
(22, 'De Firsta Epynoya', 'Padang', '1985-01-01', 'S1', 'Ahli Programmer', 1),
(23, 'Dedy Yonanta', 'Bekasi', '1987-05-26', 'S1', 'AhliDatabase Administrator', 1),
(24, 'Dhuhanawuri Agung Nugroho, S.Kom.', '', '1983-10-29', 'S1', 'Ahli Infrastruktur dan Jaringan', 1),
(25, 'Didin Wahyudin', '', '0000-00-00', 'S1', 'Ahli Programmer', 1),
(26, 'Eduard Sumual,S.Kom', 'Jakarta', '1978-11-02', 'S1', 'Ahli Web Design', 1),
(27, 'Elizabeth, S.Kom.', 'Wonogiri', '1978-12-12', 'S1', 'Ahli Administration Database', 1),
(28, 'Eri Juhari,S.Kom', 'Jakarta', '1987-02-13', 'S1', 'Operator Komputer', 1),
(29, 'Erwin Umar ', 'Jakarta', '1968-06-14', 'S1', 'AhliDatabase Administrator', 1),
(30, 'Farisya Setiadi, S.T., M.T.I', 'Jakarta', '1983-03-30', 'S2', 'Ahli Sistem Analis', 1),
(31, 'Feby Kelviandaru,S.T', 'Jakarta', '1979-02-07', 'S1', 'Operator Komputer', 1),
(32, 'Ganesha Dwi Muharso, S.Kom.', '', '1982-11-21', 'S1', 'Ahli Infrastruktur dan Jaringan', 1),
(33, 'Gulvarendi', 'Jakarta', '1974-08-26', 'S1', 'AhliDatabase Administrator', 1),
(34, 'Harits Hamid Balfast, ST.', 'Bandung ', '1983-01-17', 'S1', 'Ahli Security', 1),
(35, 'Hendo Yuwono,S.T', 'Cirebon,', '1975-03-02', 'S1', 'Ahli Sistem Analis', 1),
(36, 'Hendrikus Zebua', 'GunungSitoli', '1988-02-24', 'S1', 'AhliDatabase Administrator', 1),
(37, 'Heru Kristadi,S.Kom', 'Klaten', '1968-04-10', 'S1', 'Operator Komputer', 1),
(38, 'Imam Subarkah,Amd', 'Jakarta', '1991-01-09', 'D3', 'Operator Komputer', 1),
(39, 'Iwan Setiawan,S.T', 'Sumedang', '1971-12-22', 'S1', 'Ahli Security', 1),
(40, 'Joni Farizal, S.Kom.', 'Surabaya', '1984-06-07', 'S1', 'Ahli Programmer', 1),
(41, 'Kodrat Mahatma', ' Surakarta', '1967-11-23', 'S2', 'Ahli Sistem Engineer', 1),
(42, 'Leonard Partogi Hasoloan Siahaan, SE', 'Medan', '1965-05-28', 'S1', 'Ahli Programmer', 1),
(43, 'M. Rizky Avesena, S.Kom.', '', '1983-09-22', 'S1', 'Ahli Sistem Analis', 1),
(44, 'Muhaemin, S.Kom. MM.,M.Kom.', 'Indramayu', '1974-02-10', 'S2', 'Ahli Sistem Analis,Team Leader', 1),
(45, 'Muhamad Zuliansyah,S.T', 'Surabaya', '1975-07-20', 'S1', 'Key Management System', 1),
(46, 'Muhammad Andrey Amrulloh, A.Md.', 'Jakarta', '1993-06-14', 'D3', 'Administrasi Operator ', 1),
(47, 'Muhammad Azhari Fadli , S.T', 'Indramayu', '1988-01-05', 'S1', 'Operator Komputer', 1),
(48, 'Muhammad Hidayatullah Sumanto, S.Si.', 'Surabaya', '1989-12-03', 'S1', 'Administrator Perkantoran', 1),
(49, 'Nico Syafrizal,S.T., MM', 'P. Brandan', '1970-04-07', 'S2', 'Key Management System', 1),
(51, 'Novita Susanti, S.Kom.', 'Sukabumi', '1987-09-25', 'S1', 'Operator Komputer', 1),
(52, 'Nur Alam, S.Kom.', '', '1981-08-18', 'S1', 'Ahli Infrastruktur dan Jaringan', 1),
(53, 'Rachmat Arifin', '', '0000-00-00', 'S1', 'Ahli Programmer', 1),
(54, 'Raden Sjarif Aburrachman Ahmad,S.T., M.Sc', 'Bandung', '1970-12-16', 'S2', 'Ahli Sistem Analis', 1),
(55, 'Rahmat Kurniawan,S.Kom', 'Banda Aceh', '1978-08-29', 'S1', 'Ahli Administration Database', 1),
(56, 'Rakean Wisnudharma', 'Bandung', '1980-01-27', 'S1', 'AhliDatabase Administrator', 1),
(57, 'Randi Darmawan,Amd', 'Jakarta', '1992-03-03', 'D3', 'Operator Komputer', 1),
(58, 'Rio Kurniawan', '', '0000-00-00', 'S1', 'Ahli Programmer', 1),
(59, 'Rizki Rinaldi Akbar', '', '0000-00-00', 'S1', 'Ahli Programmer', 1),
(60, 'Rizky Purnawan Dwi Putra, S.Tr. Kom.', 'Surabaya', '1990-12-16', 'S1', 'Operator Komputer', 1),
(61, 'Robbi Baskoro, S.Kom. M.Kom.', 'Malang,', '1985-05-07', 'S2', 'Key Management System', 1),
(62, 'Rondi Torowitan', '', '0000-00-00', 'S1', 'Ahli Programmer', 1),
(63, 'Rosyidi ', 'Malang ', '1981-06-22', 'D1', 'Operator Komputer', 1),
(64, 'Rusli. S.Kom.', 'Jakarta', '1968-10-04', 'S1', 'Ahli Infrastruktur dan Jaringan', 1),
(65, 'Salies Aprilyanto,S.T', 'Surabaya', '1974-04-27', 'S1', 'Ahli Web Design', 1),
(66, 'Siti Zubaedah,S.T', 'Bandung', '1984-09-22', 'S1', 'Ahli Programmer', 1),
(67, 'Sjaeful Afandi', '', '0000-00-00', 'S1', 'Ahli Programmer', 1),
(68, 'Syahli Lilipali Sitorus,S.Kom', 'Asahan', '1983-04-30', 'S1', 'Ahli Sistem Analis', 1),
(69, 'Syaipul, S.Kom.', 'Jakarta', '1976-12-21', 'S1', 'Ahli Programmer', 1),
(70, 'Tjipto Soehardjo,S.T., M.MT', 'Kediri', '1970-10-25', 'S2', 'Ahli Security', 1),
(71, 'Tjong Kuswandi,S.T', 'Jakarta', '1974-12-17', 'S2', 'Ahli Security', 1),
(72, 'Victor Sinata', 'Jakarta', '1987-04-20', 'S1', 'AhliDatabase Administrator', 1),
(73, 'Wachyu Hari Haji,S.Kom,.M.M', 'Wonogiri', '1978-12-12', 'S1', 'Ahli Programmer', 1),
(74, 'Waskito Budi Utomo,S.Kom', 'Bandung', '1975-12-28', 'S1', 'Ahli Infrastruktur dan Jaringan', 1),
(75, 'Widodo, S.Kom. ', 'Kutoarjo', '1975-05-05', 'S1', 'AhliDatabase Administrator', 1),
(76, 'Wirman Kurniawan,S.T', 'Jakarta', '1975-12-04', 'S1', 'Ahli Web Design', 1),
(77, 'Yanti Yulianti,S.T', 'Bandung', '1977-03-05', 'S1', 'Ahli Infrastruktur dan Jaringan', 1),
(78, 'Yoke Simeon Santoso', 'Jakarta', '1991-05-28', 'S1', 'Ahli Administration Database', 1),
(79, 'Yunianto,S.T', 'Pasuruan', '1979-06-05', 'S1', 'Ahli Administration Database', 1),
(80, 'Yunita Darmastuti,S.Kom', 'Jakarta', '1988-06-23', 'S1', 'Ahli Administration Database', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tenaga_ahli_pendidikan`
--

CREATE TABLE IF NOT EXISTS `tenaga_ahli_pendidikan` (
  `id_ta_pendidikan` int(11) NOT NULL AUTO_INCREMENT,
  `id_tenaga_ahli` int(11) NOT NULL,
  `pendidikan` varchar(50) NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `universitas` varchar(50) NOT NULL,
  `tahun_ijasah` int(11) NOT NULL,
  PRIMARY KEY (`id_ta_pendidikan`),
  KEY `id_tenaga_ahli` (`id_tenaga_ahli`),
  KEY `id_tenaga_ahli_2` (`id_tenaga_ahli`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=93 ;

--
-- Dumping data untuk tabel `tenaga_ahli_pendidikan`
--

INSERT INTO `tenaga_ahli_pendidikan` (`id_ta_pendidikan`, `id_tenaga_ahli`, `pendidikan`, `jurusan`, `universitas`, `tahun_ijasah`) VALUES
(1, 1, 'D3', 'Teknik Elektro', '', 2012),
(2, 2, 'S1', 'Teknik Informatika', 'Institute Teknologi Bandung', 1998),
(3, 3, 'S1', 'Sistem Informasi', 'Institute Teknologi Surabaya', 2007),
(4, 4, 'S2', 'Elektrikal Engineering', 'Syracuse University', 1999),
(5, 4, 'S1', 'Teknik Elektro', '', 1985),
(6, 5, 'S1', 'Sistem Informasi', ' Institute Teknologi Surabaya', 2007),
(7, 6, 'S1', 'Sistem Informasi', 'Universitas Gunadarma', 2005),
(8, 6, 'S1', 'Teknik Elektro', '', 1992),
(9, 7, 'S2', 'Magister Ilmu Komputer', 'Universitas Budi Luhur', 2012),
(10, 7, 'S1', 'Sistem Informasi', 'Universitas Budi Luhur', 2006),
(11, 8, 'S2', 'Teknologi Informasi', '', 2008),
(12, 8, 'S1', 'Matematika', '', 2005),
(13, 9, 'S2', 'Magister Ilmu Komputer', '', 2009),
(14, 9, 'S1', 'Sarjana Komputer', '', 2003),
(15, 10, 'S1', 'Sistem Informasi', 'Institute Teknologi Surabaya', 2009),
(16, 11, 'S1', 'Teknik Informatika', '', 2006),
(17, 12, 'S1', 'Teknik Informatika', 'Sekolah Tinggi Sains dan Teknologi Indonesia', 2005),
(18, 13, 'S1', 'Teknik Informatika', 'Politeknik Elektronika Negeri Surabaya', 2014),
(19, 14, 'S1', 'MIPA Fisika', 'Universitas Brawijaya', 1994),
(20, 16, 'S1', 'Teknik Informatika', '', 2005),
(21, 18, 'S1', 'Sarjana Teknik Elektro', '', 2000),
(22, 19, 'S2', 'Magister Komputer', '', 0),
(23, 19, 'S1', 'Teknik Informatika', '', 1997),
(24, 20, 'S1', 'teknik industri', 'Universitas Gunadarma', 1999),
(25, 21, 'S1', 'Teknik Informatika', '', 1997),
(26, 22, 'S1', 'Sistem Informasi', '', 2008),
(27, 23, 'S1', 'Teknik Informatika', '', 2008),
(28, 24, 'S1', 'Teknik Informatika', '', 2005),
(29, 25, 'S1', 'Teknik Informatika', 'Universitas Kebangsaan', 2005),
(30, 26, 'S1', 'Sistem Informasi', '', 2001),
(31, 27, 'S1', 'Sistem Informasi', 'Universitas Tarumanegara ', 2005),
(32, 28, 'S1', 'Sistem Informasi', 'STMIK Jayakarta', 2012),
(33, 29, 'S1', 'Manajemen Informatika', '', 1994),
(34, 30, 'S2', 'Magister Teknologi Informasi', '', 2010),
(35, 30, 'S1', 'Teknik Informatika', 'Universitas Gunadarma ', 2005),
(36, 31, 'S1', 'Teknik Informatika', 'Universitas Gunadarma', 2011),
(37, 32, 'S1', 'Teknik Informatika', '', 2006),
(38, 33, 'S1', 'Teknik Informatika', '', 2001),
(39, 34, 'S1', 'Teknik Informatika', '', 2006),
(40, 35, 'S1', 'Teknik Informatika', 'Universitas Pasundan', 2002),
(41, 36, 'S1', 'Teknik Informatika', '', 2011),
(42, 37, 'S1', 'Sistem Informasi', 'Universitas Budi Luhur', 2000),
(43, 38, 'D3', 'Teknik Komputer', 'AMIK BSI', 2013),
(44, 39, 'S1', 'Teknik Informatika', 'ST Inten Bandung', 2005),
(45, 40, 'S1', 'Sistem Informai', 'Institute Teknologi Surabaya', 2007),
(46, 41, 'S2', 'Magister Ilmu Komputer', '', 2005),
(47, 41, 'S1', 'Teknik Informatika', '', 1995),
(48, 42, 'S1', 'Manajemen', 'Universitas Sumatera Utara ', 1989),
(49, 43, 'S1', 'Teknik Informatika', '', 2005),
(50, 44, 'S2', 'Ilmu Komputer', 'Universitas Budi Luhur', 2011),
(51, 44, 'S2', 'Manajemen', 'Universitas Trisakti', 2008),
(52, 44, 'S1', 'Manajemen Informatika', 'STIMIK Indonesia', 1998),
(53, 45, 'S1', 'Teknik Informatika', 'Sekolah Tinggi Teknologi Telkom', 1998),
(54, 46, 'D3', 'Teknik Informatika', '', 2014),
(55, 47, 'S1 ', 'Teknik Informatika', 'Universitas Gunadarma', 2010),
(56, 48, 'S1', 'Sistem Informasi', 'Universitas Narotama', 2013),
(57, 49, 'S2', 'Magister Manajemen', '', 2006),
(58, 49, 'S1', 'Teknik Informatika', 'Institute Teknologi Bandung', 1996),
(59, 51, 'S1', 'Sistem Informasi', 'STMIK Indonesia', 2011),
(60, 52, 'S1', 'Sistem Informasi', '', 2011),
(61, 53, 'S1', 'Teknik Informatika', 'Institute Teknologi Bandung', 2012),
(62, 54, 'S2', 'Magister of Science', 'University of Pittsburgh', 2003),
(63, 54, 'S1', 'Teknik Informatika', 'Institute Teknologi Bandung', 1997),
(64, 55, 'S1', 'Ilmu Komputer', 'Universitas Indonesia', 2007),
(65, 56, 'S1', 'Teknik Informatika', '', 2003),
(66, 57, 'D3', 'Manajemen Informatika', 'AMIK BSI', 2012),
(67, 58, 'S1', 'Teknik Informatika', 'Universitas Bina Nusantara', 2010),
(68, 59, 'S1', 'Sistem Informasi', 'Universitas Budi Luhur', 2015),
(69, 60, 'S1', 'Terapan Komputer', 'Politeknik Elektronika Negeri Surabaya', 2013),
(70, 61, 'S2', 'Teknik Elektro Multimedia', 'Institute Teknologi Surabaya', 2009),
(71, 61, 'S1', 'Sistem Informasi', 'Institute Teknologi Surabaya', 2007),
(72, 62, 'S1', 'Teknik Informatika', 'Universitas Persada Indonesia Y.A.I', 2002),
(73, 63, 'D1', 'Informatika Komputer', '', 2012),
(74, 64, 'S1', 'Sistem Informasi ', '', 2011),
(75, 65, 'S1', 'Teknik Elektro', '', 2000),
(76, 66, 'S1', 'Teknik Infromatika', 'STTIS', 2008),
(77, 67, 'S1 ', 'Teknik Informatika', 'Sekolah tinggi Sains dan Teknologi Indonesia (ST. ', 1999),
(78, 68, 'S1', 'Ilmu Komputer', 'Universitas Gajah Mada', 2009),
(79, 69, 'S1', 'Sistem Informasi', 'STMIK Indonesia', 2000),
(80, 70, 'S2', 'Manajemen Teknologi', 'Institute Teknologi Surabaya', 2002),
(81, 70, 'S1', 'Teknik Mesin', 'Universitas Brawijaya', 1996),
(82, 71, 'S2', 'Teknik informatika', 'Universitas Trisakti', 2008),
(83, 72, 'S1', 'Teknik Informatika', '', 2009),
(84, 73, 'S2', 'Magister Manajemen', '', 2009),
(85, 73, 'S1', 'Sistem Informasi', '', 2005),
(86, 74, 'S1', 'Teknik Informatika', 'STMIK AMIK Bandung', 2004),
(87, 75, 'S1', 'Sistem Informasi', 'STIMIK Perbanas', 2004),
(88, 76, 'S1', 'Teknik Elektro', '', 2000),
(89, 77, 'S1', 'Teknik Informatika', 'Universitas Pasundan', 2002),
(90, 78, 'S1', 'Sistem Informatika ', '', 2013),
(91, 79, 'S1', 'Teknik Eleketronika', 'Institute Teknologi Sepuluh Nopember', 2003),
(92, 80, 'S1', 'Sistem Informasi', 'STMIK Nusa Mandiri', 2011);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1461739449, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
