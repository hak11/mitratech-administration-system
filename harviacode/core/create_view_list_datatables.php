<?php 

$string = "
        <div class=\"row\" style=\"margin-bottom: 10px\">
            <div class=\"col-md-4 infolist\">
                <h2 style=\"margin-top:0px\">".ucfirst($table_name)." List</h2>
            </div>
            <div class=\"col-md-4 text-center\">
                <div style=\"margin-top: 4px\" id=\"message\">
                    <?php echo \$this->session->userdata('message') <> '' ? \$this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class=\"col-md-4 text-right\">
                <?php echo anchor(site_url('".$c_url."/create'), 'Tambah Data', 'class=\"btn btn-success hvr-float-shadow\" style=\"position:relative\" title=\"Tambah Data\"'); ?>";
if ($export_excel == '1') {
    $string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/excel'), 'Export Excel', 'class=\"btn btn-primary hvr-float-shadow\" style=\"position:relative\"'); ?>";
}
if ($export_word == '1') {
    $string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/word'), 'Export Word', 'class=\"btn btn-primary hvr-float-shadow\" style=\"position:relative\"'); ?>";
}
if ($export_pdf == '1') {
    $string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/pdf'), 'Export PDF', 'class=\"btn btn-danger hvr-float-shadow\" style=\"position:relative\"'); ?>";
}
$string .= "\n\t    </div>
        </div>
        <table class=\"table table-bordered table-striped\" id=\"table\">
            <thead>
                <tr>
                    <th width=\"80px\">No</th>";
foreach ($non_pk as $row) {
    $string .= "\n\t\t    <th>" . label($row['column_name']) . "</th>";
}
$string .= "\n\t\t    <th style='width:100px'>Action</th>
                </tr>
            </thead>";
$string .= "\n\t    <tbody>
            </tbody>
        </table>
        <script src=\"<?php echo base_url('assets/plugins/datatables/jquery.dataTables.js') ?>\"></script>
        <script src=\"<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.js') ?>\"></script>
        <script type=\"text/javascript\">
            var table;
            $(document).ready(function () {
              $('.btn').animate({right: '40px'});
              $('.breadcrumb').animate({top: '10px'});
              $('.infolist').animate({left: '40px'});
             table =  $(\"#table\").DataTable({ 

            \"processing\": true, //Feature control the processing indicator.
            \"serverSide\": true, //Feature control DataTables' server-side processing mode.
            \"order\": [], //Initial no order.

            \"ajax\": {
                \"url\": \"<?php echo base_url(\$this->router->fetch_class()); ?>/ajax_list\",
                \"type\": \"POST\"
            },

            //Set column definition initialisation properties.
            \"columnDefs\": [
            { 
                \"targets\": [ -1 ], //last column
                \"orderable\": false, //set not orderable
            },
            ],

        });
            });


        function delete_action(id)
            {
                  if(confirm('Are you sure delete this data?'))
                  {
                      // ajax delete data to database
                      $.ajax({
                          url : \"<?php echo base_url(\$this->router->fetch_class()); ?>/ajax_delete/\"+id,
                          type: \"POST\",
                          dataType: \"JSON\",
                          success: function(data)
                          {
                              //if success reload ajax table
                              reload_table();
                          },
                          
                          error: function (jqXHR, textStatus, errorThrown)
                          {
                              alert('Error deleting data');
                          }
                      });

                  }
              }
              
            function reload_table()
            {
                table.ajax.reload(null,false); //reload datatable ajax 
            }
        </script>
    ";


$hasil_view_list = createFile($string, $target."views/" . $v_list_file);

?>