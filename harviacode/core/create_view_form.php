<?php 

$string = "
        <div class=\"infolist\" style=\"position:relative\">
        <h2 style=\"margin-top:0px\">".ucfirst($table_name)." <?php echo \$button ?></h2>
        </div>
        <form action=\"<?php echo \$action; ?>\" method=\"post\" class=\"form-horizontal\" id=\"form\">
        <div class=\"form-body\">";
$i = 0;
foreach ($non_pk as $row) {
    $i++;
    if ($row["data_type"] == 'text')
    {
    $string .= "\n\t    <div class=\"form-group\">
            <label class=\"control-label col-md-2\" for=\"".$row["column_name"]."\">".label($row["column_name"])." </label>
            <div class=\"col-md-10\">
            <textarea class=\"form-control\" rows=\"3\" name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" tabindex=\"".$i."\" placeholder=\"".label($row["column_name"])."\"><?php echo $".$row["column_name"]."; ?></textarea>
            <div class=\"notif_error\"><?php echo form_error('".$row["column_name"]."') ?></div>
            </div>
        </div>";
    } else
    {
    $string .= "\n\t    <div class=\"form-group\">
            <label class=\"control-label col-md-2\" for=\"".$row["data_type"]."\">".label($row["column_name"])." </label>
            <div class=\"col-md-10\">
            <input type=\"text\" class=\"form-control\" tabindex=\"".$i."\" name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" placeholder=\"".label($row["column_name"])."\" value=\"<?php echo $".$row["column_name"]."; ?>\" />
            <div class=\"notif_error\"><?php echo form_error('".$row["column_name"]."') ?></div>
            </div>
        </div>";
    }
}
$string .= "\n\t    <div class=\"form-group\"> ";
$string .= "\n\t    <div class=\"col-md-2\"></div> ";
$string .= "\n\t    <div class=\"col-md-3 control-label\"> ";
$string .= "\n\t    <input type=\"hidden\" name=\"".$pk."\" style=\"position:relative\" value=\"<?php echo $".$pk."; ?>\" /> ";
$string .= "\n\t    <button type=\"submit\" class=\"btn btn-success hvr-glow\" style=\"position:relative\" tabindex=\"".$i."\"><?php echo \$button ?></button> ";
$string .= "\n\t    <a href=\"<?php echo site_url('".$c_url."') ?>\" class=\"btn btn-warning hvr-glow\" style=\"position:relative\">Cancel</a>";
$string .= "\n\t    </div> ";
$string .= "\n\t    </div> ";
$string .= "\n\t    </div> ";
$string .= "\n\t    </form> ";
$string .= "\n\t    
        <script type=\"text/javascript\">
        /*
        jQuery.validator.setDefaults({
          debug: true,
          success: \"valid\"
          });
          var form = $( \"#form\" );
          form.validate({
          rules: { ";
                foreach ($non_pk as $row) {
                    $string .= "\n\t\t   " . $row['column_name'] . ": { 
                    required:true 
                    },";
                }            

           
$string .= "\n\t  }
          });
        */
        $(document).ready(function(){
            $('.btn').animate({right: '150px'});
            $('.infolist').animate({left: '20px'});
            $('.form-group').animate({top: '20px'});
        });
        </script>  ";

$hasil_view_form = createFile($string, $target."views/" . $v_form_file);

?>