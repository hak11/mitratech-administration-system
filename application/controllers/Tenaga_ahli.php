<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tenaga_ahli extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        $this->load->model('Tenaga_ahli_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->template->build('tenaga_ahli_list');
    }

    public function ajax_list()
    {
        $list = $this->Tenaga_ahli_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $nilaiku) {
            $no++;
            $row = array();
            $row[] = $no;

		   $row[]= $nilaiku->nama;
		   $row[]= $nilaiku->tempat_lahir;
		   $row[]= $nilaiku->tanggal_lahir;
		   $row[]= $nilaiku->pendidikan_terakhir;
		   $row[]= $nilaiku->keahlian;
		   $row[]= $nilaiku->kondisi;
	        $row[] = '<a class="btn btn-sm btn-warning hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/read/'.$nilaiku->id_tenaga_ahli.'" title="Lihat Detail"><i class="glyphicon glyphicon-eye-open"></i></a>
                  <a class="btn btn-sm btn-info hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/update/'.$nilaiku->id_tenaga_ahli.'" title="Ubah Data"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger hvr-float-shadow"  href="javascript:void(0)" onclick="delete_action('."'".$nilaiku->id_tenaga_ahli."'".')" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>';


            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Tenaga_ahli_model->count_all(),
                        "recordsFiltered" => $this->Tenaga_ahli_model->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function read($id) 
    {
        $row = $this->Tenaga_ahli_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_tenaga_ahli' => $row->id_tenaga_ahli,
		'nama' => $row->nama,
		'tempat_lahir' => $row->tempat_lahir,
		'tanggal_lahir' => $row->tanggal_lahir,
		'pendidikan_terakhir' => $row->pendidikan_terakhir,
		'keahlian' => $row->keahlian,
		'kondisi' => $row->kondisi,
	    );
            $this->template->build('tenaga_ahli_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tenaga_ahli'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tenaga_ahli/create_action'),
	    'id_tenaga_ahli' => set_value('id_tenaga_ahli'),
	    'nama' => set_value('nama'),
	    'tempat_lahir' => set_value('tempat_lahir'),
	    'tanggal_lahir' => set_value('tanggal_lahir'),
	    'pendidikan_terakhir' => set_value('pendidikan_terakhir'),
	    'keahlian' => set_value('keahlian'),
	    'kondisi' => set_value('kondisi'),
	);
        $this->template->build('tenaga_ahli_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'tempat_lahir' => $this->input->post('tempat_lahir',TRUE),
		'tanggal_lahir' => $this->input->post('tanggal_lahir',TRUE),
		'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir',TRUE),
		'keahlian' => $this->input->post('keahlian',TRUE),
		'kondisi' => $this->input->post('kondisi',TRUE),
	    );

            $this->Tenaga_ahli_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tenaga_ahli'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tenaga_ahli_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tenaga_ahli/update_action'),
		'id_tenaga_ahli' => set_value('id_tenaga_ahli', $row->id_tenaga_ahli),
		'nama' => set_value('nama', $row->nama),
		'tempat_lahir' => set_value('tempat_lahir', $row->tempat_lahir),
		'tanggal_lahir' => set_value('tanggal_lahir', $row->tanggal_lahir),
		'pendidikan_terakhir' => set_value('pendidikan_terakhir', $row->pendidikan_terakhir),
		'keahlian' => set_value('keahlian', $row->keahlian),
		'kondisi' => set_value('kondisi', $row->kondisi),
	    );
            $this->template->build('tenaga_ahli_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tenaga_ahli'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_tenaga_ahli', TRUE));
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'tempat_lahir' => $this->input->post('tempat_lahir',TRUE),
		'tanggal_lahir' => $this->input->post('tanggal_lahir',TRUE),
		'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir',TRUE),
		'keahlian' => $this->input->post('keahlian',TRUE),
		'kondisi' => $this->input->post('kondisi',TRUE),
	    );

            $this->Tenaga_ahli_model->update($this->input->post('id_tenaga_ahli', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tenaga_ahli'));
        }
    }


    public function ajax_delete($id)
    {
        
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($this->Tenaga_ahli_model->delete_by_id($id,'id_tenaga_ahli')));
    }
    
    public function delete($id) 
    {
        $row = $this->Tenaga_ahli_model->get_by_id($id);

        if ($row) {
            $this->Tenaga_ahli_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tenaga_ahli'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tenaga_ahli'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required');
	$this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'trim|required');
	$this->form_validation->set_rules('pendidikan_terakhir', 'pendidikan terakhir', 'trim|required');
	$this->form_validation->set_rules('keahlian', 'keahlian', 'trim|required');
	$this->form_validation->set_rules('kondisi', 'kondisi', 'trim|required');

	$this->form_validation->set_rules('id_tenaga_ahli', 'id_tenaga_ahli', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tenaga_ahli.xls";
        $judul = "tenaga_ahli";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama");
	xlsWriteLabel($tablehead, $kolomhead++, "Tempat Lahir");
	xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Lahir");
	xlsWriteLabel($tablehead, $kolomhead++, "Pendidikan Terakhir");
	xlsWriteLabel($tablehead, $kolomhead++, "Keahlian");
	xlsWriteLabel($tablehead, $kolomhead++, "Kondisi");

	foreach ($this->Tenaga_ahli_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tempat_lahir);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tanggal_lahir);
	    xlsWriteLabel($tablebody, $kolombody++, $data->pendidikan_terakhir);
	    xlsWriteLabel($tablebody, $kolombody++, $data->keahlian);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kondisi);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

