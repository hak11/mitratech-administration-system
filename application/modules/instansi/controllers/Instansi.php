<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Instansi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        $this->load->model('Instansi_model');
        $this->load->library('form_validation');

    }

    public function index()
    {
        $this->template->build('instansi_list');
    }

    public function ajax_list()
    {
        $list = $this->Instansi_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $nilaiku) {
            $no++;
            $row = array();
            $row[] = $no;

           $row[]= $nilaiku->nama_instansi;
		   $row[]= '<img src="'.base_url('uploads/foto').'/'.$nilaiku->logo.'" width="100px">';
	        $row[] = '<a class="btn btn-sm btn-warning hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/read/'.$nilaiku->id_instansi.'" title="Lihat Detail"><i class="glyphicon glyphicon-eye-open"></i></a>
                  <a class="btn btn-sm btn-info hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/update/'.$nilaiku->id_instansi.'" title="Ubah Data"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger hvr-float-shadow"  href="javascript:void(0)" onclick="delete_action('."'".$nilaiku->id_instansi."'".')" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>';


            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Instansi_model->count_all(),
                        "recordsFiltered" => $this->Instansi_model->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function read($id) 
    {
        $row = $this->Instansi_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_instansi' => $row->id_instansi,
		'nama_instansi' => $row->nama_instansi,
	    );
            $this->template->build('instansi_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('instansi'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('instansi/create_action'),
	    'id_instansi' => set_value('id_instansi'),
        'nama_instansi' => set_value('nama_instansi'),
        'logo' => set_value('logo'),
	    'method_input' => "add",
	);
        $this->template->build('instansi_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->input->post('logo',TRUE)) {
            $logo = $this->input->post('logo',TRUE);
        } else {
            $logo = "noimage.jpg";
        }

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_instansi' => $this->input->post('nama_instansi',TRUE),
        'logo' => $logo,

	    );

            $this->Instansi_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('instansi'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Instansi_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('instansi/update_action'),
		'id_instansi' => set_value('id_instansi', $row->id_instansi),
		'nama_instansi' => set_value('nama_instansi', $row->nama_instansi),
        'logo' => set_value('logo',$row->logo),
        'method_input' => "edit",
	    );
            $this->template->build('instansi_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('instansi'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_instansi', TRUE));
        } else {
            $data = array(
        'nama_instansi' => $this->input->post('nama_instansi',TRUE),
		'logo' => $this->input->post('logo',TRUE),
	    );

            $this->Instansi_model->update($this->input->post('id_instansi', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('instansi'));
        }
    }


    public function ajax_delete($id)
    {
        
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($this->Instansi_model->delete_by_id($id,'id_instansi')));
    }
    
    public function delete($id) 
    {
        $row = $this->Instansi_model->get_by_id($id);

        if ($row) {
            $this->Instansi_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('instansi'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('instansi'));
        }
    }

    public function uploadCheck()
    {
        $baseproject ="/mitratechapp";
        $targetFolder = '/uploads/foto'; // Relative to the root and should match the upload folder in the uploader script

        if (file_exists($isi = $_SERVER['DOCUMENT_ROOT'].$baseproject.$targetFolder . '/' . $_POST['filename'])) {
            echo 1;
        } else {
            echo 0;
        }
    }


    public function upload()
    {
        
        $targetFolder = './uploads/foto'; // Relative to the root

        $verifyToken = md5('unique_salt' . $_POST['timestamp']);

        if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = $targetFolder;
            $new_filename = $verifyToken.$_FILES['Filedata']['name'];
            $targetFile = rtrim($targetPath, '/') . '/' . $new_filename;
            $nama_file = $_FILES['Filedata']['name'];
            
            // Validate the file type
            $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
            $fileParts = pathinfo($_FILES['Filedata']['name']);
            
            if (in_array($fileParts['extension'],$fileTypes)) {
                move_uploaded_file($tempFile,$targetFile);
                $array = array(
                    'foto' => $new_filename,
                );
                
                $this->session->set_flashdata( $array );
                echo $new_filename;
            } else {
                echo 'Invalid file type.';
            }
        }
    }



    public function _rules() 
    {
	$this->form_validation->set_rules('nama_instansi', 'nama instansi', 'trim|required');

	$this->form_validation->set_rules('id_instansi', 'id_instansi', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

