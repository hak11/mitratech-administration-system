
        <div class="infolist" style="position:relative">
        <h2 style="margin-top:0px">Klien Pemerintah (Instansi) <?php echo $button ?></h2>
        </div>
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal" id="form" enctype="multipart/form-data" accept-charset="utf-8">
        <div class="form-body">
         <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama Instansi </label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="1" name="nama_instansi" id="nama_instansi" placeholder="Nama Instansi" value="<?php echo $nama_instansi; ?>" />
            <div class="notif_error"><?php echo form_error('nama_instansi') ?></div>
            </div>
         </div>
	       <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Logo </label>
            <div class="col-md-10 uploadimg">
            <input id="file_upload" type="file" name="logo"  multiple="false" />
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-2" for="varchar"></label>
            <div class="col-md-4">
            <?php if ($method_input!="add") {
              if ($logo!='noimage.jpg'){ ?>
              <img src="<?php echo base_url('uploads/foto').'/'.$logo ?>" id="user_photo" width="200px">
              <?php } else { ?>
              <img src="<?php echo base_url('uploads/foto').'/noimage.jpg' ?>" id="user_photo" width="200px">  
              <?php } 
            } else { ?>
              <img src="<?php echo base_url('uploads/foto').'/noimage.jpg' ?>" id="user_photo" width="200px">
            <?php } ?>

            <button type="button" class="btn btn-danger cancelimg" onclick="removeimg()"><i class='fa fa-trash'></i></button>
            <div class="notif_error"><div id="queue"></div></div>

            </div>
         </div>
	    <div class="form-group"> 
	    <div class="col-md-2"></div> 
	    <div class="col-md-3 control-label"> 
      <input type="hidden" name="id_instansi" style="position:relative" value="<?php echo $id_instansi; ?>" /> 
	    <input type="hidden" name="logo" style="position:relative" value="<?php echo $logo; ?>" id="logo" /> 
	    <button type="submit" class="btn btn-success hvr-glow" style="position:relative" tabindex="1"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('instansi') ?>" class="btn btn-warning hvr-glow" style="position:relative">Cancel</a>
	    </div> 
	    </div> 
	    </div> 
	    </form> 
	    
        <script type="text/javascript">
        /*
        jQuery.validator.setDefaults({
          debug: true,
          success: "valid"
          });
          var form = $( "#form" );
          form.validate({
          rules: { 
		   nama_instansi: { 
                    required:true 
                    },
	  }
          });
        */
       var method_input;
       var logoku;

       method_input = "<?php echo $method_input ?>";
       logoku = "<?php echo $logo ?>";

        $(document).ready(function(){
            $('.btn').animate({right: '150px'});
            $('.infolist').animate({left: '20px'});
            $('.form-group').animate({top: '20px'});
            if (method_input=="add" || logoku=="noimage.jpg") {
            $('.cancelimg').hide();                            
            } else if(method_input=="edit"){
            $(".uploadimg").hide('slow');
            }

        });
        <?php $timestamp = time();?>

        $(function() {
            $('#file_upload').uploadifive({
              'auto'             : true,
              'checkScript'      : '<?php echo base_url('instansi/uploadCheck') ?>',
              'formData'         : {
                           'timestamp' : '<?php echo $timestamp;?>',
                           'token'     : '<?php echo md5('unique_salt' . $timestamp);?>'

                                   },
              'progressData' : 'speed',
              'multi'            : false,
              'method'           : 'post',
              'swf'      : '<?php echo base_url('assets'); ?>/plugins/uploadjquery/uploadify.swf',
              'queueID'          : 'queue',
              'uploadScript'     : '<?php echo base_url('instansi/upload') ?>',
              'onUploadComplete' : function(file, data) { 

                                  $("#user_photo").attr("src", "<?php echo base_url(); ?>/uploads/foto/"+data);
                                  $("#logo").attr("value", data);
                                  $(".uploadimg").hide('slow');
                                  $(".cancelimg").show('slow');

                                    console.log(file);
                                    console.log(data);
                                  }
            });
          });

        function removeimg(){
          $("#user_photo").attr("src", "<?php echo base_url(); ?>/uploads/foto/noimage.jpg");
          $("#logo").attr("value", "noimage.jpg");
          $(".uploadimg").show('slow');
          $(".cancelimg").hide('slow');
          $(".complete").hide('slow');
        }
        </script>  