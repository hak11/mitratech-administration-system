<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tenaga_ahli extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        $this->load->model('Tenaga_ahli_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['list_pendidikan'] = $this->Tenaga_ahli_model->get_select_group('pendidikan_terakhir','tenaga_ahli');

        $this->template->build('tenaga_ahli_list',$data);
    }

    public function ajax_list()
    {
        $list = $this->Tenaga_ahli_model->get_datatables();
        $data = array();
        $array_pendidikan = array();
        $no = $_POST['start'];
        foreach ($list as $nilaiku) {
            $no++;
            $row = array();

            $convert_tgl = date("d-m-Y",strtotime($nilaiku->tanggal_lahir));
            $val_pendidikan = $this->Tenaga_ahli_model->find_by_id($nilaiku->id_tenaga_ahli,"id_tenaga_ahli","tenaga_ahli_pendidikan");
            foreach ($val_pendidikan as $val) {
              if ($val->id_ta_pendidikan) {
                $array_pendidikan[] = "<div>
                            <td>".$val->pendidikan."</td>
                            <td>".$val->jurusan."</td>
                            <td>".$val->universitas."</td>
                            <td>".$val->tahun_ijasah."</td>
                          </div>";
              } else {
                $array_pendidikan[] = '<tr><td>Tidak Ada Keterangan</td></tr>';
              }

            }

    		   $row[]= $nilaiku->nama;

    		   $row[]= $convert_tgl;
    		   $row[]= $array_pendidikan;

           if ($nilaiku->kondisi) {
            $row[] = "Aktiv";
            $kondisi = "style='background-color:red'";
           } else {
            $row[] = "-" ;
            $kondisi = "style='background-color:red'";
           }
	        $row[] = '<a class="btn btn-sm btn-warning hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/read/'.$nilaiku->id_tenaga_ahli.'" title="Lihat Detail"><i class="glyphicon glyphicon-eye-open"></i></a>
                  <a class="btn btn-sm btn-info hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/update/'.$nilaiku->id_tenaga_ahli.'" title="Ubah Data"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger hvr-float-shadow"  href="javascript:void(0)" onclick="delete_action('."'".$nilaiku->id_tenaga_ahli."'".')" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>';


          $data[] = $row;
          $array_pendidikan= "";
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Tenaga_ahli_model->count_all(),
                        "recordsFiltered" => $this->Tenaga_ahli_model->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function read($id)
    {
        $row = $this->Tenaga_ahli_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_tenaga_ahli' => $row->id_tenaga_ahli,
		'nama' => $row->nama,
		'tempat_lahir' => $row->tempat_lahir,
		'tanggal_lahir' => $row->tanggal_lahir,
		'pendidikan_terakhir' => $row->pendidikan_terakhir,
		'keahlian' => $row->keahlian,
		'kondisi' => $row->kondisi,
	    );
            $this->template->build('tenaga_ahli_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tenaga_ahli'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tenaga_ahli/create_action'),
	    'id_tenaga_ahli' => set_value('id_tenaga_ahli'),
	    'nama' => set_value('nama'),
	    'tempat_lahir' => set_value('tempat_lahir'),
	    'tanggal_lahir' => set_value('tanggal_lahir'),
	    'pendidikan_terakhir' => set_value('pendidikan_terakhir'),
	    'keahlian' => set_value('keahlian'),
	    'kondisi' => set_value('kondisi'),
      'save_method' => "add",
	);
        $this->template->build('tenaga_ahli_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $tanggal_lahir = date("Y-m-d",strtotime($this->input->post('tanggal_lahir')));


            $data = array(
        		'nama' => $this->input->post('nama',TRUE),
        		'tempat_lahir' => $this->input->post('tempat_lahir',TRUE),
        		'tanggal_lahir' => $tanggal_lahir,
        		'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir',TRUE),
        		'keahlian' => $this->input->post('keahlian',TRUE),
        		'kondisi' => $this->input->post('kondisi',TRUE),
        	    );

            $id_ta = $this->Tenaga_ahli_model->insert($data);
            if (empty($id_ta)) {
              $id_ta=2;
            }
            if ($this->input->post('pendidikan')) {
                $pendidikan = $this->input->post('pendidikan');
                foreach ($pendidikan as $key => $value) {
                  $pendidikanku = array(
                    'id_tenaga_ahli' => $id_ta,
                    'pendidikan' => $this->input->post('pendidikan')[$key],
                    'jurusan' => $this->input->post('jurusan')[$key],
                    'universitas' => $this->input->post('universitas')[$key],
                    'tahun_ijasah' => $this->input->post('tahun_ijasah')[$key],
                  );

                  $this->Tenaga_ahli_model->insert_general($pendidikanku);
                  $pendidikanku = "";
                }
            }
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tenaga_ahli'));
        }
        echo "<hr />";
        print_r($data);
        echo "<hr />";
        echo "wew";
    }

    public function update($id)
    {
        $row = $this->Tenaga_ahli_model->get_by_id($id);

        if ($row) {

          $val_pendidikan = $this->Tenaga_ahli_model->find_by_id($row->id_tenaga_ahli,"id_tenaga_ahli","tenaga_ahli_pendidikan");
          foreach ($val_pendidikan as $val) {
            if ($val->id_ta_pendidikan) {
              $array_pendidikan[] = "<div>
                          <td>".$val->pendidikan."</td>
                          <td>".$val->jurusan."</td>
                          <td>".$val->universitas."</td>
                          <td>".$val->tahun_ijasah."</td>
                        </div>";
            } else {
              $array_pendidikan[] = '<tr><td>Tidak Ada Keterangan</td></tr>';
            }
          }


            $data = array(
                'button' => 'Update',
                'action' => site_url('tenaga_ahli/update_action'),
		'id_tenaga_ahli' => set_value('id_tenaga_ahli', $row->id_tenaga_ahli),
		'nama' => set_value('nama', $row->nama),
		'tempat_lahir' => set_value('tempat_lahir', $row->tempat_lahir),
		'tanggal_lahir' => set_value('tanggal_lahir', $row->tanggal_lahir),
		'pendidikan_terakhir' => set_value('pendidikan_terakhir', $row->pendidikan_terakhir),
		'keahlian' => set_value('keahlian', $row->keahlian),
		'kondisi' => set_value('kondisi', $row->kondisi),
		'pendidikan_list' => $array_pendidikan,
    'save_method' => "edit",

	    );
            $this->template->build('tenaga_ahli_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tenaga_ahli'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_tenaga_ahli', TRUE));
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'tempat_lahir' => $this->input->post('tempat_lahir',TRUE),
		'tanggal_lahir' => $this->input->post('tanggal_lahir',TRUE),
		'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir',TRUE),
		'keahlian' => $this->input->post('keahlian',TRUE),
		'kondisi' => $this->input->post('kondisi',TRUE),
	    );

            $this->Tenaga_ahli_model->update($this->input->post('id_tenaga_ahli', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tenaga_ahli'));
        }
    }

    public function ajax_view($id)
    {
        $list = $this->Tenaga_ahli_model->get_by_id_custom("*,tenaga_ahli.nama as tenaga_ahli_nama","tenaga_ahli_pendidikan",$id,"tenaga_ahli_pendidikan.id_tenaga_ahli","tenaga_ahli","tenaga_ahli_pendidikan.id_tenaga_ahli=tenaga_ahli.id_tenaga_ahli");

        foreach ($list as $val) {
          if ($val->id_ta_pendidikan) {
            $row[] = "<tr>
                        <td>".$val->id_ta_pendidikan."</td>
                        <td>".$val->tenaga_ahli_nama."</td>
                        <td>".$val->pendidikan."</td>
                        <td>".$val->jurusan."</td>
                        <td>".$val->universitas."</td>
                        <td>".$val->tahun_ijasah."</td>
                      </tr>";
          } else {
            $row[] = '<tr><td>Tidak Ada Keterangan</td></tr>';
          }

        }

        echo json_encode($row);
    }

    public function ajax_delete($id)
    {

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($this->Tenaga_ahli_model->delete_by_id($id,'id_tenaga_ahli')));
    }

    public function delete($id)
    {
        $row = $this->Tenaga_ahli_model->get_by_id($id);

        if ($row) {
            $this->Tenaga_ahli_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tenaga_ahli'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tenaga_ahli'));
        }
    }

    public function ajax_filter($id_pendikan,$kondisi)
    {

        if ($id_pendikan=="all" AND $kondisi=="all") {
            $list = $this->Tenaga_ahli_model->get_datatables();
        } else if($id_pendikan!="all" AND $kondisi == "all"){
            $list = $this->Tenaga_ahli_model->get_datatable_filter($id_pendikan,"pendidikan_terakhir");
        } else if($id_pendikan=="all" AND $kondisi != "all"){
            $list = $this->Tenaga_ahli_model->get_datatable_filter($kondisi,"kondisi");
        } else if($id_pendikan!="all" AND $kondisi!="all"){
            $list = $this->Tenaga_ahli_model->get_datatable_filter_multi($id_pendikan,$kondisi);
        }


        $data = array();
        $array_pendidikan = array();
        $no = $_POST['start'];
        foreach ($list as $nilaiku) {
            $row = array();
            $val_pendidikan = $this->Tenaga_ahli_model->find_by_id($nilaiku->id_tenaga_ahli,"id_tenaga_ahli","tenaga_ahli_pendidikan");
            foreach ($val_pendidikan as $val) {
              if ($val->id_ta_pendidikan) {
                $array_pendidikan[] = "<div>
                            <td>".$val->pendidikan."</td>
                            <td>".$val->jurusan."</td>
                            <td>".$val->universitas."</td>
                            <td>".$val->tahun_ijasah."</td>
                          </div>";
              } else {
                $array_pendidikan[] = '<tr><td>Tidak Ada Keterangan</td></tr>';
              }

            }


           $row[]= $nilaiku->nama;
           $row[]= $nilaiku->tanggal_lahir;
           $row[]= $array_pendidikan;
           if ($nilaiku->kondisi) {
            $row[] = "Aktiv";
            $kondisi = "style='background-color:red'";
           } else {
            $row[] = "-" ;
            $kondisi = "style='background-color:red'";
           }
            $row[] = '<a class="btn btn-sm btn-warning hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/read/'.$nilaiku->id_tenaga_ahli.'" title="Lihat Detail"><i class="glyphicon glyphicon-eye-open"></i></a>
                  <a class="btn btn-sm btn-info hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/update/'.$nilaiku->id_tenaga_ahli.'" title="Ubah Data"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger hvr-float-shadow"  href="javascript:void(0)" onclick="delete_action('."'".$nilaiku->id_tenaga_ahli."'".')" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>';


            $data[] = $row;
            $array_pendidikan = "";
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Tenaga_ahli_model->count_all(),
                        "recordsFiltered" => $this->Tenaga_ahli_model->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required');
	$this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'trim|required');
	$this->form_validation->set_rules('pendidikan_terakhir', 'pendidikan terakhir', 'trim|required');
	$this->form_validation->set_rules('keahlian', 'keahlian', 'trim|required');
	$this->form_validation->set_rules('kondisi', 'kondisi', 'trim|required');

	$this->form_validation->set_rules('id_tenaga_ahli', 'id_tenaga_ahli', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tenaga_ahli.xls";
        $judul = "tenaga_ahli";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama");
	xlsWriteLabel($tablehead, $kolomhead++, "Tempat Lahir");
	xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Lahir");
	xlsWriteLabel($tablehead, $kolomhead++, "Pendidikan Terakhir");
	xlsWriteLabel($tablehead, $kolomhead++, "Keahlian");
	xlsWriteLabel($tablehead, $kolomhead++, "Kondisi");

	foreach ($this->Tenaga_ahli_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tempat_lahir);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tanggal_lahir);
	    xlsWriteLabel($tablebody, $kolombody++, $data->pendidikan_terakhir);
	    xlsWriteLabel($tablebody, $kolombody++, $data->keahlian);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kondisi);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}
