<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tenaga_ahli_model extends CI_Model
{

    var $table = 'tenaga_ahli';
    public $id = 'id_tenaga_ahli';
    public $order = 'DESC';
    var $column = array('tenaga_ahli.id_tenaga_ahli','tenaga_ahli.nama');
    var $cs_order = array('id_tenaga_ahli' => 'desc');

    function __construct()
    {
        parent::__construct();
    }

        private function _get_datatables_query()
    {
        $this->db->select('*,tenaga_ahli_pendidikan.tahun_ijasah as ijasah,tenaga_ahli_pendidikan.id_tenaga_ahli as kode_tenaga_ahli')
        ->from($this->table)
        ->join('tenaga_ahli_pendidikan','tenaga_ahli_pendidikan.id_tenaga_ahli=tenaga_ahli.id_tenaga_ahli','left')
        ->group_by('tenaga_ahli.id_tenaga_ahli');

        $i = 0;

        foreach ($this->column as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {

                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; // set column array variable to order processing
            $i++;
        }
        //
        // if(isset($_POST['order'])) // here order processing
        // {
        //     $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        // }
        // else if(isset($this->order))
        // {
        //     $order = $this->cs_order;
        //     $this->db->order_by(key($order), $order[key($order)]);
        // }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_datatable_filter($id,$pkey)
        {
            $this->db->select('*')->from($this->table)->where($pkey,$id);
            $i = 0;

        foreach ($this->column as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {

                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; // set column array variable to order processing
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->cs_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

            $query = $this->db->get();
            return $query->result();
        }

    function get_datatable_filter_multi($pendidikan_terakhir,$kondisi)
        {
            $this->db->select('*')->from($this->table)->where("pendidikan_terakhir",$pendidikan_terakhir)->where("kondisi",$kondisi);
        $i = 0;

        foreach ($this->column as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {

                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; // set column array variable to order processing
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->cs_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
            $query = $this->db->get();
            return $query->result();
        }


    function get_select_option($col,$tbl)
    {
        $this->db->select($col)->from($tbl);
        $query = $this->db->get();
        return $query->result();
    }

    function get_select_group($col,$tbl)
    {
        $this->db->select($col)->from($tbl)->group_by('pendidikan_terakhir');
        $query = $this->db->get();
        return $query->result();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }


    public function delete_by_id($id,$pkey)
    {
        $this->db->where($pkey,$id);
        $this->db->delete($this->table);
        return array("status" => true);
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get data by id
    function find_by_id($id,$primary,$fromtable)
    {
        $this->db->select('*');
        $this->db->from($fromtable);
        $this->db->where($primary, $id);
        $query = $this->db->get();
        return $query->result();
    }

     public function get_by_id_custom($col,$table,$id,$pkey,$jointable=null,$join=null)
    {
        $this->db->select($col)->from($table);
        if ($join) {
          $this->db->join($jointable,$join,'left');
        }
        $this->db->where($pkey,$id);
        $query = $this->db->get();

        return $query->result();
    }



    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_tenaga_ahli', $q);
	$this->db->or_like('nama', $q);
	$this->db->or_like('tempat_lahir', $q);
	$this->db->or_like('tanggal_lahir', $q);
	$this->db->or_like('pendidikan_terakhir', $q);
	$this->db->or_like('keahlian', $q);
	$this->db->or_like('kondisi', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_tenaga_ahli', $q);
	$this->db->or_like('nama', $q);
	$this->db->or_like('tempat_lahir', $q);
	$this->db->or_like('tanggal_lahir', $q);
	$this->db->or_like('pendidikan_terakhir', $q);
	$this->db->or_like('keahlian', $q);
	$this->db->or_like('kondisi', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);

        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }
    // insert data
    function insert_general($data)
    {
        $this->db->insert("tenaga_ahli_pendidikan", $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}
