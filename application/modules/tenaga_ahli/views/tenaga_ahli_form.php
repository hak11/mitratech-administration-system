<?php
$timestamp = time();
// if ($save_method=="add") {
//   $mytokenis = md5('unique_salt' . $timestamp);
// } else {
//   $mytokenis = $file_lampiran;
// }
?>
        <div class="infolist" style="position:relative">
        <h2 style="margin-top:0px">Tenaga Ahli <?php echo $button ?></h2>
        </div>
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal" id="form">
        <div class="form-body">
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama </label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="1" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
            <div class="notif_error"><?php echo form_error('nama') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Tempat Lahir </label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="2" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" value="<?php echo $tempat_lahir; ?>" />
            <div class="notif_error"><?php echo form_error('tempat_lahir') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="date">Tanggal Lahir </label>
            <div class="col-md-10">
            <input type="text" class="form-control datepicker" tabindex="3" name="tanggal_lahir" id="tanggal_lahir" placeholder="Tanggal Lahir" value="<?php echo $tanggal_lahir; ?>" />
            <div class="notif_error"><?php echo form_error('tanggal_lahir') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Pendidikan Terakhir </label>
            <div class="col-md-10">
            <select class="form-control" name="pendidikan_terakhir" id="pendidikan_terakhir" tabindex="4">
              <option value="">-- Pilih --</option>
              <option value="S3" <?php if($pendidikan_terakhir == "S3"){ ?> selected <?php } ?>>S3</option>
              <option value="S2" <?php if($pendidikan_terakhir == "S2"){ ?> selected <?php } ?>>S2</option>
              <option value="S1" <?php if($pendidikan_terakhir == "S1"){ ?> selected <?php } ?>>S1</option>
              <option value="D3" <?php if($pendidikan_terakhir == "D3"){ ?> selected <?php } ?> >D3</option>
              <option value="D1" <?php if($pendidikan_terakhir == "D1"){ ?> selected <?php } ?>>D1</option>
              <option value="SMA" <?php if($pendidikan_terakhir == "SMA"){ ?> selected <?php } ?>>SMA</option>
            </select>
            <div class="notif_error"><?php echo form_error('pendidikan_terakhir') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="keahlian">Keahlian </label>
            <div class="col-md-10">
            <textarea class="form-control" rows="3" name="keahlian" id="keahlian" tabindex="5" placeholder="Keahlian"><?php echo $keahlian; ?></textarea>
            <div class="notif_error"><?php echo form_error('keahlian') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="tinyint">Kondisi </label>
            <div class="col-md-10">
            <select class="form-control" tabindex="6" id="kondisi" name="kondisi">
              <option value="1">Aktiv</option>
              <option value="0">Non Aktiv</option>
            </select>
            <div class="notif_error"><?php echo form_error('kondisi') ?></div>
            </div>
      </div>
      <div class="form-group text-right">
        <div style="padding-right:15px;">
        <INPUT id="tambah_data" type="button" value="Tambah Data" class="btn btn-success" onclick="addRow()" name="tambah_row" />
        </div>
      </div>
      <div class="form-group">
          <div class="col-md-12">
          <table class="table table-striped table-border" id="mtableform">
          <thead>
          <tr>
            <th width="120px">Pendidikan</th>
            <th width="250px">Jurusan</th>
            <th width="250px">Universitas</th>
            <th width="120px">Tahun Ijasah</th>
          </tr>
        </thead>
          <?php
          if (isset($isi_data)) {
            foreach ($nilaisaya as $key) {
          ?>
            <tr id="<?php echo $key->id_ta_pendidikan ?>">
              <td><?php echo $key->pendidikan ?></td>
              <td><?php echo $key->jurusan ?></td>
              <td><?php echo $key->universitas ?></td>
              <td><?php echo $key->tahun_ijasah ?></td>
              <td width="120px;">
                <button type="button" name="hapus_data" class="btn btn-danger" onclick="hapusData('<?php echo $key->id_dashboard_penyelesaian_masalah ?>')"><span class="fa fa-trash"></span></button>
                <button type="button" name="edit_data" class="btn btn-primary" onclick="editData('<?php echo $key->id_dashboard_penyelesaian_masalah ?>')"><span class="fa fa-pencil"></span></button>
              </td>
            </tr>
          <?php
            }
          }
          ?>
          <tbody>
          <tr id="form_tambah">
            <td>
              <select class="form-control" name="pendidikan[]" tabindex="4">
                <option value="">-- Pilih --</option>
                <option value="S3" <?php if($pendidikan_terakhir == "S3"){ ?> selected <?php } ?>>S3</option>
                <option value="S2" <?php if($pendidikan_terakhir == "S2"){ ?> selected <?php } ?>>S2</option>
                <option value="S1" <?php if($pendidikan_terakhir == "S1"){ ?> selected <?php } ?>>S1</option>
                <option value="D3" <?php if($pendidikan_terakhir == "D3"){ ?> selected <?php } ?> >D3</option>
                <option value="D1" <?php if($pendidikan_terakhir == "D1"){ ?> selected <?php } ?>>D1</option>
                <option value="SMA" <?php if($pendidikan_terakhir == "SMA"){ ?> selected <?php } ?>>SMA</option>
              </select>
            </td>
            <td><input type="text" class="form-control" name="jurusan[]" placeholder="Jurusan Yang Diambil" tabindex="4"></td>
            <td><input type="text" class="form-control" name="universitas[]" placeholder="Nama Universitas" tabindex="5"></td>
            <td><input type="number" class="form-control" name="tahun_ijasah[]" placeholder="2000"></td>
          </tr>
          <?php if ($save_method != "add") { ?>
            <tr id="form_edit">
              <td>
                <select class="form-control" name="pendidikan" tabindex="4">
                  <option value="">-- Pilih --</option>
                  <option value="S3" <?php if($pendidikan_terakhir == "S3"){ ?> selected <?php } ?>>S3</option>
                  <option value="S2" <?php if($pendidikan_terakhir == "S2"){ ?> selected <?php } ?>>S2</option>
                  <option value="S1" <?php if($pendidikan_terakhir == "S1"){ ?> selected <?php } ?>>S1</option>
                  <option value="D3" <?php if($pendidikan_terakhir == "D3"){ ?> selected <?php } ?> >D3</option>
                  <option value="D1" <?php if($pendidikan_terakhir == "D1"){ ?> selected <?php } ?>>D1</option>
                  <option value="SMA" <?php if($pendidikan_terakhir == "SMA"){ ?> selected <?php } ?>>SMA</option>
                </select>
              </td>
              <td><input type="text" class="form-control" name="jurusan" placeholder="Jurusan Yang Diambil" tabindex="4"></td>
              <td><input type="text" class="form-control" name="universitas" placeholder="Nama Universitas" tabindex="5"></td>
              <td><input type="number" class="form-control" name="tahun_ijasah" placeholder="2000"></td>
              <td>
                <button type="button" name="SimpanEdit" id="simpanEdit" class="btn btn-success sembunyikan" onclick="simpanvalEdit()"><span class="fa fa-check"></span></button>
                <button type="button" name="CancelEdit" id="cancelEdit" class="btn btn-warning sembunyikan" onclick="cancelvalEdit()"><span class="fa fa-ban"></span></button>
              </td>
            </tr>
          <?php } ?>
          <tbody>
          </table>
          <?php if (isset($pendidikan_list)) {
            print_r($pendidikan_list);
          } ?>
          </div>
      </div>
	    <div class="form-group">
	    <div class="col-md-2"></div>
	    <div class="col-md-3 control-label">
	    <input type="hidden" name="id_tenaga_ahli" style="position:relative" value="<?php echo $id_tenaga_ahli; ?>" />
	    <button type="submit" class="btn btn-success hvr-glow" style="position:relative" tabindex="6"><?php echo $button ?></button>
	    <a href="<?php echo site_url('tenaga_ahli') ?>" class="btn btn-warning hvr-glow" style="position:relative">Cancel</a>
	    </div>
	    </div>
	    </div>
	    </form>

        <script type="text/javascript">
        $('.datepicker').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            todayHighlight: true,
            orientation: "top auto",
            todayBtn: true,
            todayHighlight: true,
        });

         $(".datepicker").datepicker().datepicker("setDate", new Date());


        /*
        jQuery.validator.setDefaults({
          debug: true,
          success: "valid"
          });
          var form = $( "#form" );
          form.validate({
          rules: {
		   nama: {
                    required:true
                    },
		   tempat_lahir: {
                    required:true
                    },
		   tanggal_lahir: {
                    required:true
                    },
		   pendidikan_terakhir: {
                    required:true
                    },
		   keahlian: {
                    required:true
                    },
		   kondisi: {
                    required:true
                    },
	  }
          });
        */
        $(document).ready(function(){
            $('.btn').animate({right: '150px'});
            $('.infolist').animate({left: '20px'});
            $('.form-group').animate({top: '20px'});
        });

        $(function() {
            $('.file_upload').uploadifive({
              'height'        : 30,
              'width'         : 120,
              'auto'             : true,
              'checkScript'      : '<?php echo base_url('project/uploadCheck') ?>',
              'onAddQueueItem' : function(file) {
                  this.data('uploadifive').settings.formData = {
              'someKey' : $(this).attr("name"),
              'timestamp' : '<?php echo $timestamp;?>',
              'token'     : '<?php echo md5('unique_salt' . $timestamp) ?>',
              'keterangan' : '<?php echo $save_method ?>'
                };
              },
              'progressData' : 'speed',
              'multi'            : true,
              'method'           : 'post',
              'swf'      : '<?php echo base_url('assets'); ?>/plugins/uploadjquery/uploadify.swf',
              'queueID'          : 'queue',
              'uploadScript'     : '<?php echo base_url('project/upload') ?>',
              'onCancel'     : function(file) {
                // alert($('input[name="'+file.name+'"]').val());
                var id_lampiran = $('#temp_' + file.name.replace(/[^a-zA-Z0-9]/g,'_'));
                id_lampiran.remove();

                $.ajax({
                 type: "POST",
                 url: '<?php echo base_url('project/hapus_upload') ?>',
                 data:id_lampiran,
                 success: function(data){
                   console.log(data);
                 },

                 error:function(XMLHttpRequest){
                   alert("Data Tidak Dapat Ditemukan");
                 }

               })


              },
              'onUploadComplete' : function(file, data) {
                                    if (data=="outoflength") {
                                      alert("Nama file melebihi batas masuk");
                                    } else if(data=='infalidfiletype'){
                                      alert("File yang anda upload tidak valid");
                                    } else {
                                    $('#form').append('<input id="temp_' + file.name.replace(/[^a-zA-Z0-9]/g,'_') + '" class="form-control '+ file.name.replace(/[^a-zA-Z0-9]/g,'_') +'" type="hidden" name="file_lampiran[]" value="' + data +'" />');
                                    }
              }
            });
          });

          function cancel_before(id){
            var nama = $("#isi_file_edit_"+id).val();

            var dataini = {
              id:id,
              nama:nama,
              keterangan:"hapus_cancel"
            }

            $.ajax({
             type: "POST",
             url: '<?php echo base_url('project/hapus_upload') ?>',
             data:dataini,
             success: function(data){
               if (data=="successdel") {
                  $("#isi_file_edit_"+id).remove();
                  $("#file_edit_"+id).remove();
               }
             },

             error:function(XMLHttpRequest){
               alert("Data Tidak Dapat Ditemukan");
             }

           })
          }

          function addRow() {

                  var isi = '<tr>\
                                <td><select class="form-control" name="pendidikan[]" id="pendidikan_terakhir" tabindex="4">\
                                  <option value="">-- Pilih --</option>\
                                  <option value="S3">S3</option>\
                                  <option value="S2">S2</option>\
                                  <option value="S1">S1</option>\
                                  <option value="D3">D3</option>\
                                  <option value="D1">D1</option>\
                                  <option value="SMA">SMA</option>\
                                </select></td>\
                                <td><input type="text" class="form-control" name="jurusan[]" placeholder="Jurusan Yang Diambil" tabindex="4"></td>\
                                <td><input type="text" class="form-control" name="universitas[]" placeholder="Nama Universitas" tabindex="5"></td>\
                                <td><input type="number" class="form-control" name="tahun_ijasah[]" placeholder="2000"></td>\
                              </tr>';

                    $('#mtableform').append(isi);
                }
        </script>
