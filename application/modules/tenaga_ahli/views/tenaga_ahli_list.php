<div id="modal_resume" class="modal fade col-md-12" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header </h4>
      </div>
      <div class="modal-body">
      <div class="row text-center">
       <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body box-profile text-center">
              <table class="table table-striped table-bordered" id="isi_lampiran">
                <thead>
                  <tr>
                    <th>Nama File</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div> <!-- box-body -->
          </div>
        </div>


      </div> <!-- row -->
      <div class="modal-footer">
        <div id="editEmployeeButton"></div>
      </div>

      </div> <!-- modal-body -->
      </div> <!-- modal-content -->
    </div> <!-- modal-dialog -->
  </div> <!-- modal-resume -->


        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4 infolist">
                <h2 style="margin-top:0px">Tenaga Ahli List</h2>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php echo anchor(site_url('tenaga_ahli/create'), 'Tambah Data', 'class="btn btn-success hvr-float-shadow" style="position:relative" title="Tambah Data"'); ?>
		<?php echo anchor(site_url('tenaga_ahli/excel'), 'Export Excel', 'class="btn btn-primary hvr-float-shadow" style="position:relative"'); ?>
	    </div>
        </div>
    </div>

     <div class="col-md-4">
          <div class="form-group">
          <label for="exampleInputEmail1">CARI BERDASARKAN NAMA</label>
          <input type="text" class="form-control" id="nama" placeholder="Nama">
        </div>
    </div>
    <div class="col-md-2">
          <div class="form-group">
          <label>PENDIDIKAN</label>
          <select class="form-control" id="pilih_pendidikan">
            <option value="all">-- Pilih Semua --</option>
             <?php foreach ($list_pendidikan as $value): ?>
                <option value="<?php echo $value->pendidikan_terakhir ?>"><?php echo $value->pendidikan_terakhir ?></option>
            <?php endforeach; ?>
          </select>
          </div>
    </div>
    <div class="col-md-3">
          <div class="form-group">
          <label>STATUS</label>
          <select class="form-control" id="pilih_status">
            <option value="all">-- Pilih Semua --</option>
            <option value="1">Aktiv</option>
          </select>
          </div>
    </div>
    <div class="col-md-3">
          <div class="form-group">
          <label>&nbsp</label>
          <button type="button" class="form-control btn btn-primary" id="cari">Cari</button>
          </div>
    </div>
        <table class="table table-bordered table-striped text-center" id="table">
            <thead>
                <tr>
		    <th>Nama</th>
        <th>Tanggal Lahir</th>
		    <th>Pendidikan</th>
		    <th>Status</th>
		    <th style='width:100px'>Action</th>
                </tr>
            </thead>
	    <tbody>
            </tbody>
        </table>


        <div id="modal_detail" class="modal fade col-md-12" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail Pendidikan </h4>

              </div>
              <div class="modal-body">
              <table class="table table-striped" id="mtableform">

              </table>
              </div>
            </div>
          </div>
        </div>
        <script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            var table;
            $(document).ready(function () {
              $('.btn').animate({right: '40px'});
              $('.breadcrumb').animate({top: '10px'});
              $('.infolist').animate({left: '40px'});
             table =  $("#table").DataTable({

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            "ajax": {
                "url": "<?php echo base_url($this->router->fetch_class()); ?>/ajax_list",
                "type": "POST"
            },

            //Set column definition initialisation properties.
            "columnDefs": [
            {
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],

        });

             $('#nama').keyup(function(){
              table.search($(this).val()).draw() ;
            })

             $('#cari').click(function(){
            table.ajax.url( '<?php echo base_url($this->router->fetch_class()); ?>/ajax_filter/' + $('#pilih_pendidikan').val() + "/" + $('#pilih_status').val()).load();
            });
            });


        function delete_action(id)
            {
                  if(confirm('Are you sure delete this data?'))
                  {
                      // ajax delete data to database
                      $.ajax({
                          url : "<?php echo base_url($this->router->fetch_class()); ?>/ajax_delete/"+id,
                          type: "POST",
                          dataType: "JSON",
                          success: function(data)
                          {
                              //if success reload ajax table
                              reload_table();
                          },

                          error: function (jqXHR, textStatus, errorThrown)
                          {
                              alert('Error deleting data');
                          }
                      });

                  }
              }

            function reload_table()
            {
                table.ajax.reload(null,false); //reload datatable ajax
            }



            function view_action(id)
  {
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string

      //Ajax Load data from ajax
      $.ajax({
          url : "<?php echo base_url($this->router->fetch_class()); ?>/ajax_view/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

              // var isi = '<tr id="myTableRow">\
              //     <td>\
              //     <input type="text" name="tanggal[]" class="form-control" placeholder="20-05-2011"></td>\
              //     <td><input type="text" name="info[]" class="form-control" placeholder="ada info untuk sesuatu"></td>\
              //     </td>\
              //   </tr>';

            // $('#mtableform').append(data);
            //
            $('#isi_lampiran').html(data);
            $('#modal_resume').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Lampiran'); // Set title to Bootstrap modal title
            console.log(data);

              // $('#modal_detail').modal('show'); // show bootstrap modal when complete loaded

          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
  }
        </script>
