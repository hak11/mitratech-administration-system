<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Project extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        $this->load->model('Project_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    public function index()
    {
        $data['status_project'] = 'ongoing';
        $this->template->build('project_list',$data);
    }

    public function finish()
    {
        $data['status_project'] = 'finish';
        $this->template->build('project_list',$data);
    }

    public function ajax_list($param)
    {
        $list = $this->Project_model->get_datatables($param);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $nilaiku) {
        $tanggal_pekerjaan = date("d-m-Y",strtotime($nilaiku->tanggal_pekerjaan));


        $val_status = $nilaiku->status_rekan;
        if ($val_status==true) {
            $nama_rekan = $this->Project_model->lihat_nama_rekanan('nama_rekanan as nama_rekanan_val','rekanan_swasta','id_rekanan_swasta',$nilaiku->id_rekan);
        } else {
            $nama_rekan = $this->Project_model->lihat_nama_rekanan('instansi.nama_instansi as nama_rekanan_val,rekanan.nama_direktorat as nama_direktorat_val','rekanan','id_rekanan',$nilaiku->id_rekan,'instansi','instansi.id_instansi=rekanan.nama_instansi');
        }


            $no++;
            $row = array();
        if (isset($nama_rekan)) {
          if ($val_status==true) {
             $row[]= $nama_rekan->nama_rekanan_val;
          } else {
             $row[]= $nama_rekan->nama_rekanan_val.' <br />( '.$nama_rekan->nama_direktorat_val.')';
          }
        }

		   $row[]= $nilaiku->nama_pekerjaan;
		   $row[]= $nilaiku->lokasi_pekerjaan;
		   $row[]= '<button class="btn bg-olive hvr-float-shadow" onclick="view_lampiran('."'".$nilaiku->id_project."'".')">'.$tanggal_pekerjaan.'</button>';
		   $row[]= '<a class="btn btn-sm btn-block bg-purple btn-flat hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/read/'.$nilaiku->link.'" title="Lihat Detail"><i class="fa fa-link"></i></a>';
	        $row[] = '<a class="btn btn-sm btn-warning hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/read/'.$nilaiku->id_project.'" title="Lihat Detail"><i class="glyphicon glyphicon-eye-open"></i></a>
                  <a class="btn btn-sm btn-info hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/update/'.$nilaiku->id_project.'" title="Ubah Data"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger hvr-float-shadow"  href="javascript:void(0)" onclick="delete_action('."'".$nilaiku->id_project."'".')" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>';


            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Project_model->count_all(),
                        "recordsFiltered" => $this->Project_model->count_filtered($param),
                        "data" => $data,
                );
        echo json_encode($output);
    }


    public function read($id)
    {
        $row = $this->Project_model->get_by_id($id);

        if ($row->status_rekan==1) {
          $rekan = "Swasta";
        } else {
          $rekan = "Instansi Pemerintah";
        }

        $pagu_kontrak = "Rp " . number_format($row->nilai_kontrak,2,',','.');

        if ($row) {
            $data = array(
		'id_project' => $row->id_project,
		'status_rekan' => $rekan,
		'nama_pekerjaan' => $row->nama_pekerjaan,
		'bidang_pekerjaan' => $row->bidang_pekerjaan,
		'lokasi_pekerjaan' => $row->lokasi_pekerjaan,
		'no_pekerjaan' => $row->no_pekerjaan,
		'tanggal_pekerjaan' => $row->tanggal_pekerjaan,
		'nilai_kontrak' => $pagu_kontrak,
		'mata_uang' => $row->mata_uang,
		'tanggal_rencana_mulai' => $row->tanggal_rencana_mulai,
		'tanggal_rencana_akhir' => $row->tanggal_rencana_akhir,
		'link' => $row->link,
	    );
            $this->template->build('project_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('project'));
        }
    }

    public function create()
    {

        $data = array(
            'button' => 'Create',
            'action' => site_url('project/create_action'),
	    'id_project' => set_value('id_project'),
	    'perusahaan' => set_value('perusahaan'),
	    'status_rekan' => set_value('status_rekan'),
	    'nama_pekerjaan' => set_value('nama_pekerjaan'),
	    'bidang_pekerjaan' => set_value('bidang_pekerjaan'),
	    'lokasi_pekerjaan' => set_value('lokasi_pekerjaan'),
	    'no_pekerjaan' => set_value('no_pekerjaan'),
	    'tanggal_pekerjaan' => set_value('tanggal_pekerjaan'),
	    'nilai_kontrak' => set_value('nilai_kontrak'),
	    'mata_uang' => set_value('mata_uang'),
	    'tanggal_rencana_mulai' => set_value('tanggal_rencana_mulai'),
	    'tanggal_rencana_akhir' => set_value('tanggal_rencana_akhir'),
        'status_project' => set_value('status_project'),
        'link' => set_value('link'),
	    'save_method' => "add",
	);
        $this->template->build('project_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

        $tanggal_pekerjaan = date("Y-m-d",strtotime($this->input->post('tanggal_pekerjaan')));
        $tanggal_rencana_mulai = date("Y-m-d",strtotime($this->input->post('tanggal_rencana_mulai')));
        $tanggal_rencana_akhir = date("Y-m-d",strtotime($this->input->post('tanggal_rencana_akhir')));

        $tokenmasuk = $this->input->post('token_saya',TRUE);
        $bidang_pekerjaan = $this->input->post('bidang_pekerjaan',TRUE);
        $val_bidang_pekerjaan = implode(",", $bidang_pekerjaan);




            $data = array(
		'status_rekan' => $this->input->post('status_rekan',TRUE),
		'id_rekan' => $this->input->post('id_rekan',TRUE),
		'perusahaan' => $this->input->post('perusahaan',TRUE),
		'nama_pekerjaan' => $this->input->post('nama_pekerjaan',TRUE),
		'bidang_pekerjaan' => $val_bidang_pekerjaan,
		'lokasi_pekerjaan' => $this->input->post('lokasi_pekerjaan',TRUE),
		'no_pekerjaan' => $this->input->post('no_pekerjaan',TRUE),
		'tanggal_pekerjaan' => $tanggal_pekerjaan,
		'nilai_kontrak' => $this->input->post('nilai_kontrak',TRUE),
		'mata_uang' => $this->input->post('mata_uang',TRUE),
		'tanggal_rencana_mulai' => $tanggal_rencana_mulai,
        'tanggal_rencana_akhir' => $tanggal_rencana_akhir,
		'status_project' => $this->input->post('status_project',TRUE),
		'link' => $this->input->post('link',TRUE),
		'file_lampiran' => $tokenmasuk,
	    );

      $lampiran_masuk = $this->input->post('file_lampiran');
      foreach ($lampiran_masuk as $val) {
        $datalampiran = array(
          'token' => $this->input->post('token_saya',TRUE),
          'nama' => $val
        );

        $this->Project_model->insert_upload($datalampiran);
      }

            $this->Project_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('project/finish'));
        }
    }

    public function view_lampiran($id_project)
  	{
  		$list = $this->Project_model->get_by_id_single("lampiran_project.nama as lampiran_project_jo","project",$id_project,"id_project");
      // $data = array();
        foreach ($list as $val) {
          if ($val->lampiran_project_jo) {
            $row[] = "<tr><td><a href='./uploads/document/".$val->lampiran_project_jo."' target='_blank'>".$val->lampiran_project_jo."</a></td></tr>";
          } else {
            $row[] = '<tr><td>Tidak Ada File Lampiran</td></tr>';
          }

        }


      $data['val'] = $row;
      $data['hiden'] = $row;



        echo json_encode($data);

  	}

    public function update($id)
    {
        $row = $this->Project_model->get_by_id($id);


        if ($row->status_rekan == 1) {
            $status_rekan = "Swasta";
            $col = "nama_rekanan";
            $tbl_nama_rekan = "rekanan_swasta";
            $pkey = "id_rekanan_swasta";
            $join = null;
            $on = null;
        } else {
            $status_rekan = "Instansi Pemerintah";
            $col = "instansi.nama_instansi as nama_rekanan_val,rekanan.nama_direktorat as nama_direktorat_val";
            $tbl_nama_rekan = "rekanan";
            $pkey = "id_rekanan";
            $join = "instansi";
            $on = "instansi.id_instansi=rekanan.nama_instansi";
        }

        $nama_rekan = $this->Project_model->get_select_one_join($col,$tbl_nama_rekan,$pkey,$row->id_rekan,$join,$on);

        if ($row->status_rekan == 1) {
            $val_nama_rekan = $nama_rekan->nama_rekanan;
        } else {
            $val_nama_rekan = $nama_rekan->nama_rekanan_val." | ".$nama_rekan->nama_direktorat_val;

        }

        $list = $this->Project_model->get_by_id_single("id,lampiran_project.nama as lampiran_project_jo","project",$row->id_project,"id_project");
        $dataku = array();

        foreach ($list as $val) {
          if ($val->lampiran_project_jo) {
            $nilai = $val->lampiran_project_jo;
            $isi_lampiran = substr($nilai,18);
            $data_saya_temp[] = array(
              'id' => $val->id,
              'nama_file' => $val->lampiran_project_jo,
              'nama_file_real' => $isi_lampiran,
            );
            $keteranganfileada  = "success";
          } else {
            $data_saya_temp[] = '<tr><td>Tidak Ada File Lampiran</td></tr>';
            $keteranganfileada  = "error";
          }
        }
        $dataku=$data_saya_temp;






        $tanggal_pekerjaan = date("d-m-Y",strtotime($row->tanggal_pekerjaan));
        $tanggal_rencana_mulai = date("d-m-Y",strtotime($row->tanggal_rencana_mulai));
        $tanggal_rencana_akhir = date("d-m-Y",strtotime($row->tanggal_rencana_akhir));


        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('project/update_action'),
		'id_project' => set_value('id_project', $row->id_project),
        'status_rekan' => set_value('status_rekan', $status_rekan),
		'id_rekan' => set_value('id_rekan', $val_nama_rekan),
		'nama_pekerjaan' => set_value('nama_pekerjaan', $row->nama_pekerjaan),
		'bidang_pekerjaan' => set_value('bidang_pekerjaan', $row->bidang_pekerjaan),
		'lokasi_pekerjaan' => set_value('lokasi_pekerjaan', $row->lokasi_pekerjaan),
		'no_pekerjaan' => set_value('no_pekerjaan', $row->no_pekerjaan),
		'tanggal_pekerjaan' => set_value('tanggal_pekerjaan', $tanggal_pekerjaan),
		'nilai_kontrak' => set_value('nilai_kontrak', $row->nilai_kontrak),
		'mata_uang' => set_value('mata_uang', $row->mata_uang),
		'tanggal_rencana_mulai' => set_value('tanggal_rencana_mulai',$tanggal_rencana_mulai),
		'tanggal_rencana_akhir' => set_value('tanggal_rencana_akhir', $tanggal_rencana_akhir),
        'link' => set_value('link', $row->link),
		'status_project' => set_value('status_project', $row->status_project),
		  'file_lampiran' => set_value('file_lampiran', $row->file_lampiran),
        'save_method' => "edit",
	    );

      $data['isi_lampiran_data'] = $dataku;
      $data['keteranganfileada'] = $keteranganfileada;

            $this->template->build('project_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('project/finish'));
        }
    }

    public function update_action()
    {
        $this->_rules_update();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_project', TRUE));
        } else {


        $tanggal_pekerjaan = date("Y-m-d",strtotime($this->input->post('tanggal_pekerjaan')));
        $tanggal_rencana_mulai = date("Y-m-d",strtotime($this->input->post('tanggal_rencana_mulai')));
        $tanggal_rencana_akhir = date("Y-m-d",strtotime($this->input->post('tanggal_rencana_akhir')));

        $bidang_pekerjaan = $this->input->post('bidang_pekerjaan');
        $val_bidang_pekerjaan = implode(",", $bidang_pekerjaan);

        $tokenmasuk = $this->input->post('token_saya',TRUE);

            $data = array(
                      'perusahaan' => $this->input->post('perusahaan',TRUE),
                		  'nama_pekerjaan' => $this->input->post('nama_pekerjaan',TRUE),
                		  'bidang_pekerjaan' => $val_bidang_pekerjaan,
                		  'lokasi_pekerjaan' => $this->input->post('lokasi_pekerjaan',TRUE),
                		  'no_pekerjaan' => $this->input->post('no_pekerjaan',TRUE),
                      'tanggal_pekerjaan' => $tanggal_pekerjaan,
                		  'nilai_kontrak' => $this->input->post('nilai_kontrak',TRUE),
                		  'mata_uang' => $this->input->post('mata_uang',TRUE),
                      'tanggal_rencana_mulai' => $tanggal_rencana_mulai,
                      'tanggal_rencana_akhir' => $tanggal_rencana_akhir,
                      'status_project' => $this->input->post('status_project',TRUE),
                		  'link' => $this->input->post('link',TRUE),
              	    );

      $tampung_array1 = array();
      $list_lampiran_cek = $this->Project_model->check_on_lampiran($tokenmasuk);
      foreach ($list_lampiran_cek as $val_lampiran) {
        $row_lampiran[] = $val_lampiran;
      }

      $lampiran_masuk = $this->input->post('file_lampiran');



      $no=0;
      if (isset($lampiran_masuk)) {
          foreach ($lampiran_masuk as $val) {
                  $datalampiran = array(
                      'token' => $this->input->post('token_saya',TRUE),
                      'nama' => $val
                    );

                  $this->Project_model->insert_upload($datalampiran);
          }
      }

            echo $this->input->post('id_project', TRUE);
            $this->Project_model->update($this->input->post('id_project', TRUE), $data);
      echo "<hr />";
      echo "<pre>";
      print_r($data);
      echo "</pre>";
      echo "<hr />";

            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('project'));

        }
    }


    public function ajax_delete($id)
    {

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($this->Project_model->delete_by_id($id,'id_project')));
    }

    public function ajax_filter_rekanan()
    {
        $status = $this->input->post('status');
        if ($status==1) {
            $list = $this->Project_model->get_select_option("id_rekanan_swasta,nama_rekanan","rekanan_swasta");
        } else {
            $list = $this->Project_model->get_select_option_join("rekanan.id_rekanan as id_rekanan,instansi.nama_instansi as nama_instansi_jo,rekanan.nama_direktorat as nama_direktorat","rekanan","instansi","rekanan.nama_instansi=instansi.id_instansi");
        }


        $data = array();
        if ($list) {
        $row[]='<option value="">Pilih Klien</option>';
        $kondisi = "success";
        } else {
        $row[]='<option value="">Tidak Ada Klien Tersedia</option>';
        $kondisi = "not";
        }
        foreach ($list as $val_rekanan) {
            if ($status==1) {
            $row[] = '<option value="'.$val_rekanan->id_rekanan_swasta.'">'.$val_rekanan->nama_rekanan.'</option>';
            } else {
            $row[] = '<option value="'.$val_rekanan->id_rekanan.'">'.$val_rekanan->nama_instansi_jo.' | '.$val_rekanan->nama_direktorat.'</option>';
            }

        }

        $data['val'] = $row;

        echo json_encode($data);
    }

    public function delete($id)
    {
        $row = $this->Project_model->get_by_id($id);

        if ($row) {
            $this->Project_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('project'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('project'));
        }
    }


    public function uploadCheck()
    {
        $baseproject ="/mitratechapp";
        $targetFolder = '/uploads/foto'; // Relative to the root and should match the upload folder in the uploader script

        if (file_exists($isi = $_SERVER['DOCUMENT_ROOT'].$baseproject.$targetFolder . '/' . $_POST['filename'])) {
            echo 1;
        } else {
            echo 0;
        }
    }


    public function upload()
    {

        $targetFolder = './uploads/document'; // Relative to the root

        $verifyToken = md5('unique_salt' . $_POST['timestamp']);
        $potong_hash = substr($verifyToken,0,15);

        if (!empty($_FILES) && $_POST['token'] == $verifyToken || $_POST['keterangan'] == 'edit') {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = $targetFolder;
            $new_filename = $potong_hash.'_-_'.$_FILES['Filedata']['name'];
            $targetFile = rtrim($targetPath, '/') . '/' . $new_filename;
            $nama_file = $_FILES['Filedata']['name'];

            // Validate the file type
            $fileTypes = array('jpg','jpeg','gif','png','doc','docx','xls','xlsx','pdf'); // File extensions
            $fileParts = pathinfo($_FILES['Filedata']['name']);

            if (in_array($fileParts['extension'],$fileTypes)) {
              if (strlen($new_filename)<100) {

                move_uploaded_file($tempFile,$targetFile);

                echo $new_filename;
              } else {
                echo "outoflength";
              }
            } else {
                echo 'infalidfiletype';
            }
        }
    }

    public function hapus_upload()
    {
        $targetFolder = './uploads/document/'; // Relative to the root
        if ($this->input->post('keterangan')) {
          $nama_file_id = $this->input->post('nama');
          $id_file = $this->input->post('id');
          $this->Project_model->delete_by_id_custom($id_file,'id','lampiran_project');
        } else {
          $nama_file_id = $this->input->post('file_lampiran')[0];
        }
        if ($nama_file_id!="") {
          unlink($targetFolder.$nama_file_id);
          echo "successdel";
        } else {
          echo "terdapat kesalahan";
        }
    }

    public function _rules()
    {
    $this->form_validation->set_rules('status_rekan', 'status rekan', 'trim|required');
    $this->form_validation->set_rules('id_rekan', 'id rekan', 'trim|required');
    $this->form_validation->set_rules('nama_pekerjaan', 'nama pekerjaan', 'trim|required');
    $this->form_validation->set_rules('lokasi_pekerjaan', 'lokasi pekerjaan', 'trim|required');
    $this->form_validation->set_rules('no_pekerjaan', 'no pekerjaan', 'trim|required');
    $this->form_validation->set_rules('tanggal_pekerjaan', 'tanggal pekerjaan', 'trim|required');
    $this->form_validation->set_rules('nilai_kontrak', 'nilai kontrak', 'trim|required');
    $this->form_validation->set_rules('mata_uang', 'mata uang', 'trim|required');
    $this->form_validation->set_rules('tanggal_rencana_mulai', 'tanggal rencana mulai', 'trim|required');
    $this->form_validation->set_rules('tanggal_rencana_akhir', 'tanggal rencana akhir', 'trim|required');

    $this->form_validation->set_rules('id_project', 'id_project', 'trim');
    $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function _rules_update()
    {
	$this->form_validation->set_rules('nama_pekerjaan', 'nama pekerjaan', 'trim|required');
	$this->form_validation->set_rules('lokasi_pekerjaan', 'lokasi pekerjaan', 'trim|required');
	$this->form_validation->set_rules('no_pekerjaan', 'no pekerjaan', 'trim|required');
	$this->form_validation->set_rules('tanggal_pekerjaan', 'tanggal pekerjaan', 'trim|required');
	$this->form_validation->set_rules('nilai_kontrak', 'nilai kontrak', 'trim|required');
	$this->form_validation->set_rules('mata_uang', 'mata uang', 'trim|required');
	$this->form_validation->set_rules('tanggal_rencana_mulai', 'tanggal rencana mulai', 'trim|required');
	$this->form_validation->set_rules('tanggal_rencana_akhir', 'tanggal rencana akhir', 'trim|required');

	$this->form_validation->set_rules('id_project', 'id_project', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "project.xls";
        $judul = "project";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Rekan");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Pekerjaan");
	xlsWriteLabel($tablehead, $kolomhead++, "Bidang Pekerjaan");
	xlsWriteLabel($tablehead, $kolomhead++, "Lokasi Pekerjaan");
	xlsWriteLabel($tablehead, $kolomhead++, "No Pekerjaan");
	xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Pekerjaan");
	xlsWriteLabel($tablehead, $kolomhead++, "Nilai Kontrak");
	xlsWriteLabel($tablehead, $kolomhead++, "Mata Uang");
	xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Rencana Mulai");
	xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Rencana Akhir");
	xlsWriteLabel($tablehead, $kolomhead++, "Link");

	foreach ($this->Project_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status_rekan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_pekerjaan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->bidang_pekerjaan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->lokasi_pekerjaan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->no_pekerjaan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tanggal_pekerjaan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nilai_kontrak);
	    xlsWriteLabel($tablebody, $kolombody++, $data->mata_uang);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tanggal_rencana_mulai);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tanggal_rencana_akhir);
	    xlsWriteLabel($tablebody, $kolombody++, $data->link);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}
