<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Project_model extends CI_Model
{

    var $table = 'project';
    public $id = 'id_project';
    public $order = 'DESC';
    var $column = array('id_project');
    var $cs_order = array('id_project' => 'desc');

    function __construct()
    {
        parent::__construct();
    }

        private function _get_datatables_query($status)
    {
        $this->db->where("status_project",$status);
        $this->db->select('id_project,id_rekan,nama_pekerjaan,lokasi_pekerjaan,tanggal_pekerjaan,link,status_rekan')
        ->from($this->table);

        $i = 0;

        foreach ($this->column as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {

                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; // set column array variable to order processing
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->cs_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($param)
    {
        if ($param=="finish") {
          $status = 1;
        } else {
          $status = 0;
        }
        $this->_get_datatables_query($status);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();

        return $query->result();
    }


    function count_filtered($param)
    {
      if ($param=="finish") {
        $status = 1;
      } else {
        $status = 0;
      }
        $this->_get_datatables_query($status);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function lihat_nama_rekanan($col,$tbl,$pk,$id,$join=null,$on=null){
        if ($tbl=="rekanan_swasta") {
            $this->db->select($col)->from($tbl)
            ->where($pk,$id);
        } else {
            $this->db->select($col)->from($tbl)
            ->join($join,$on,'left')
            ->where($pk,$id);
        }

        $query = $this->db->get();
        return $query->row();
    }


    function get_select_filter($col,$table,$pkey,$id)
        {
            $this->db->select($col)->from($table)->where($pkey,$id);

            $query = $this->db->get();
            return $query->result();
        }

    function get_select_option($col,$tbl)
    {
        $this->db->select($col)->from($tbl);
        $query = $this->db->get();
        return $query->result();
    }

    function get_select_option_join($col,$tbl,$join,$on)
    {
        $this->db->select($col)->from($tbl)
        ->join($join,$on);
        $query = $this->db->get();
        return $query->result();
    }

    function get_select_one_join($col,$table,$pkey,$id,$tablejoin = null,$on = null)
    {
        if ($tablejoin) {
            $join = $this->db->join($tablejoin,$on);
        } else {
            $join = "";
        }

        $this->db->select($col)->from($table);
        $join;
        $this->db->where($pkey,$id);
        return $this->db->get()->row();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }


    public function delete_by_id($id,$pkey)
    {
        $this->db->where($pkey,$id);
        $this->db->delete($this->table);
        return array("status" => true);
    }

    public function delete_by_id_custom($id,$pkey,$table)
    {
        $this->db->where($pkey,$id);
        $this->db->delete($table);
        return array("status" => true);
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    public function get_by_id_single($select,$table,$id,$pkey)
    {
        $this->db->select($select);
        $this->db->from($table);
        $this->db->join("lampiran_project","project.file_lampiran=lampiran_project.token",'left');
        $this->db->where($pkey,$id);
        $query = $this->db->get();

        return $query->result();
    }

    public function check_on_lampiran($id){
      $this->db->select('nama');
      $this->db->from('lampiran_project');
      $this->db->where('token',$id);
      $query = $this->db->get();

      return $query->result_array();
    }


    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_project', $q);
	$this->db->or_like('status_rekan', $q);
	$this->db->or_like('nama_pekerjaan', $q);
	$this->db->or_like('bidang_pekerjaan', $q);
	$this->db->or_like('lokasi_pekerjaan', $q);
	$this->db->or_like('no_pekerjaan', $q);
	$this->db->or_like('tanggal_pekerjaan', $q);
	$this->db->or_like('nilai_kontrak', $q);
	$this->db->or_like('mata_uang', $q);
	$this->db->or_like('tanggal_rencana_mulai', $q);
	$this->db->or_like('tanggal_rencana_akhir', $q);
	$this->db->or_like('link', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_project', $q);
	$this->db->or_like('status_rekan', $q);
	$this->db->or_like('nama_pekerjaan', $q);
	$this->db->or_like('bidang_pekerjaan', $q);
	$this->db->or_like('lokasi_pekerjaan', $q);
	$this->db->or_like('no_pekerjaan', $q);
	$this->db->or_like('tanggal_pekerjaan', $q);
	$this->db->or_like('nilai_kontrak', $q);
	$this->db->or_like('mata_uang', $q);
	$this->db->or_like('tanggal_rencana_mulai', $q);
	$this->db->or_like('tanggal_rencana_akhir', $q);
	$this->db->or_like('link', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }
    // insert data lampiran
    function insert_upload($data)
    {

      $orig_debug = $this->db->db_debug;
      $this->db->db_debug = FALSE;

      if ($this->db->insert("lampiran_project", $data)) {
        $this->db->db_debug = $orig_debug;
        return true;
      } else {
        $this->db->db_debug = $orig_debug;
        return false;
      }

    }
    // update data
    function update($id, $data)
    {
      $this->db->where($this->id, $id);
      $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}
