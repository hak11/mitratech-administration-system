<?php
$timestamp = time();
if ($save_method=="add") {
  $mytokenis = md5('unique_salt' . $timestamp);
} else {
  $mytokenis = $file_lampiran;
}
?>
  <div class="infolist" style="position:relative">
        <h2 style="margin-top:0px">Project <?php echo $button ?></h2>
        </div>
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal" id="form">
        <div class="form-body">
        <div class="form-group">
            <label class="control-label col-md-2" for="tinyint">Perusahaan </label>
            <div class="col-md-10">
            <select class="form-control" name="perusahaan" tabindex="1" id="perusahaan">
                <option value="PT.MITRATECH ANDAL SINERGIA">PT.MITRATECH ANDAL SINERGIA</option>
                <option value="PT.MULIA ABADI SUPLAI">PT.MULIA ABADI SUPLAI</option>
                <option value="PT.MEGAKON AMANAH SAKTI">PT.MEGAKON AMANAH SAKTI</option>
            </select>
            <div class="notif_error"><?php echo form_error('status_rekan') ?></div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="tinyint">Status Klien </label>
            <div class="col-md-10">
            <?php if ($save_method=="add"): ?>
                <select name="status_rekan" class="form-control" tabindex="1" id="status" placeholder="Status Rekan">
                <option value="">--Pilih Status--</option>
                <option value="0">Instansi Pemerintah</option>
                <option value="1">Swasta</option>
                </select>
            <?php else: ?>
                <div class="form-control" readonly><?php echo $status_rekan; ?></div>
            <?php endif ?>

            <div class="notif_error"><?php echo form_error('status_rekan') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="tinyint">Klien </label>
            <div class="col-md-10">
            <?php if ($save_method=="add"): ?>
                <select name="id_rekan" class="form-control" tabindex="1" id="rekanan" placeholder="Status Rekan">
                <option value="">--Pilih Klien (Pilih Status Rekan)--</option>

            </select>
            <?php else: ?>
                <div class="form-control" readonly><?php echo $id_rekan; ?></div>
            <?php endif ?>

            <div class="notif_error"><?php echo form_error('id_rekan') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama Pekerjaan </label>
            <div class="col-md-10">
            <textarea type="text" class="form-control" tabindex="2" name="nama_pekerjaan" id="nama_pekerjaan" placeholder="Nama Pekerjaan"><?php echo $nama_pekerjaan; ?></textarea>
            <div class="notif_error"><?php echo form_error('nama_pekerjaan') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Bidang Pekerjaan </label>
            <div class="col-md-2">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="bidang_pekerjaan[]" value="TELEMATIKA" tabindex="3">
                        TELEMATIKA
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="bidang_pekerjaan[]" value="PEMETAAN" tabindex="3">
                        PEMETAAN
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="bidang_pekerjaan[]" value="LAINNYA" tabindex="3">
                        LAINNYA
                    </label>
                </div>
            </div>
            <div class="notif_error"><?php echo form_error('bidang_pekerjaan') ?></div>
        </div>
        <br />
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Lokasi Pekerjaan </label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="4" name="lokasi_pekerjaan" id="lokasi_pekerjaan" placeholder="Lokasi Pekerjaan" value="<?php echo $lokasi_pekerjaan; ?>" />
            <div class="notif_error"><?php echo form_error('lokasi_pekerjaan') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">No Pekerjaan </label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="5" name="no_pekerjaan" id="no_pekerjaan" placeholder="No Pekerjaan" value="<?php echo $no_pekerjaan; ?>" />
            <div class="notif_error"><?php echo form_error('no_pekerjaan') ?></div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="date">Tanggal Pekerjaan </label>
            <div class="col-md-10">
            <input type="text" class="form-control datepicker" tabindex="6" name="tanggal_pekerjaan" id="tanggal_pekerjaan" placeholder="Tanggal Pekerjaan" value="<?php echo $tanggal_pekerjaan; ?>" />
            <div class="notif_error"><?php echo form_error('tanggal_pekerjaan') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="nilai_kontrak">Nilai Kontrak </label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="7" name="nilai_kontrak" id="nilai_kontrak" placeholder="Nilai Kontrak" value="<?php echo $nilai_kontrak; ?>" />
            <div class="notif_error"><?php echo form_error('nilai_kontrak') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="mata_uang">Mata Uang </label>
            <div class="col-md-10">
            <select class="form-control" tabindex="8" name="mata_uang" id="mata_uang">
                <option value="rupiah">Rp (Rupiah)</option>
                <option value="dollar">$ (Dollar)</option>
                <option value="euro"><span class="glyphicon glyphicon-euro"></span> Euro</option>
            </select>
            <div class="notif_error"><?php echo form_error('mata_uang') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="tanggal_rencana_mulai">Tanggal Rencana Mulai </label>
            <div class="col-md-10">
            <input type="text" class="form-control datepicker" tabindex="9" name="tanggal_rencana_mulai" id="tanggal_rencana_mulai" placeholder="Tanggal Rencana Mulai" value="<?php echo $tanggal_rencana_mulai; ?>" />
            <div class="notif_error"><?php echo form_error('tanggal_rencana_mulai') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="tanggal_rencana_akhir">Tanggal Rencana Akhir </label>
            <div class="col-md-10">
            <input type="text" class="form-control datepicker" tabindex="10" name="tanggal_rencana_akhir" id="tanggal_rencana_akhir" placeholder="Tanggal Rencana Akhir" value="<?php echo $tanggal_rencana_akhir; ?>" />
            <div class="notif_error"><?php echo form_error('tanggal_rencana_akhir') ?></div>
            </div>
        </div>
      <div class="form-group">
            <label class="control-label col-md-2" for="link">Link </label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="11" name="link" id="link" placeholder="Link" value="<?php echo $link; ?>" />
            <div class="notif_error"><?php echo form_error('link') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="link">Status Poject </label>
            <div class="col-md-10">
            <select class="form-control" tabindex="12" name="status_project" id="status_project">
              <option value="1" <?php if ($status_project==1 && $save_method=="edit") { ?> selected <?php } ?> >Finished</option>
              <option value="0" <?php if ($status_project==0 && $save_method=="edit") { ?> selected <?php } ?> >On Progress</option>
            </select>
            <div class="notif_error"><?php echo form_error('status_project') ?></div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="dokument_lampiran">Dokument Lampiran</label>
            <div class="col-md-2">
            <input type="file" class="file_upload" tabindex="5" name="dokument_lampiran"  />
            <div class="notif_error"><?php echo form_error('dokument_lampiran') ?></div></div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
              <div id="queue"></div>
              <?php
              if ($save_method=="edit" && $keteranganfileada == "success") {
                if ($file_lampiran!="") {
                  $no=99;
                  // if ($isi_lampiran_data[0]['nama_file']) {
                  foreach ($isi_lampiran_data as $val) {
                  ?>
                  <div class="uploadifive-queue-item complete" id="file_edit_<?php echo $val['id']; ?>">
                    <a class="close" href="javascript:void(0)" onclick="cancel_before('<?php echo $val['id'] ?>')">X</a>
                    <div>
                      <span class="filename"><a href="./uploads/document/<?php echo $val['nama_file'] ?>" target='_blank'><?php echo $val['nama_file_real'] ?></a></span>
                    </div>
                  </div>
                  <input type="hidden" id="isi_file_edit_<?php echo $val['id']; ?>" name="file_lampiran[]" value="<?php echo $val['nama_file'] ?>">
                  <?php
                  $no++;
                  }
                  // }
                }
                ?>
              <?php
              }
              ?>
            </div>
        </div>
	    <div class="form-group">
	    <div class="col-md-2"></div>
	    <div class="col-md-3 control-label">
	    <input type="hidden" name="id_project" style="position:relative" value="<?php echo $id_project; ?>" />
	    <input type="hidden" name="token_saya" style="position:relative" value="<?php echo $mytokenis; ?>" />
	    <button type="submit" class="btn btn-success hvr-glow" style="position:relative" tabindex="11"><?php echo $button ?></button>
	    <a href="<?php echo site_url('project') ?>" class="btn btn-warning hvr-glow" style="position:relative">Cancel</a>
	    </div>
	    </div>
	    </div>
	    </form>

        <?php if ($save_method=="add"): ?>
            <script type="text/javascript">
                    $('.datepicker').datepicker({
                  autoclose: true,
                  format: "dd-mm-yyyy",
                  todayHighlight: true,
                  orientation: "top auto",
                  todayBtn: true,
                  todayHighlight: true,
              });

               $(".datepicker").datepicker().datepicker("setDate", new Date());
            </script>
        <?php else: ?>
            <script type="text/javascript">

                $(document).ready(function(){
                      var bidang_pekerjaan = "<?php echo $bidang_pekerjaan ?>";
                      var split_bidang_pekerjaan = bidang_pekerjaan.split(",");
                      var max_split_bidang_pekerjaan = split_bidang_pekerjaan.length;


                            for (var i=0; i <= max_split_bidang_pekerjaan-1; i++) {
                                $('input[type="checkbox"][value="'+split_bidang_pekerjaan[i]+'"]').prop('checked', true);
                            };

                });
            </script>
        <?php endif ?>

        <script type="text/javascript">
        /*
        jQuery.validator.setDefaults({
          debug: true,
          success: "valid"
          });
          var form = $( "#form" );
          form.validate({
          rules: {
		   status_rekan: {
                    required:true
                    },
		   nama_pekerjaan: {
                    required:true
                    },
		   bidang_pekerjaan: {
                    required:true
                    },
		   lokasi_pekerjaan: {
                    required:true
                    },
		   no_pekerjaan: {
                    required:true
                    },
		   tanggal_pekerjaan: {
                    required:true
                    },
		   nilai_kontrak: {
                    required:true
                    },
		   mata_uang: {
                    required:true
                    },
		   tanggal_rencana_mulai: {
                    required:true
                    },
		   tanggal_rencana_akhir: {
                    required:true
                    },
		   link: {
                    required:true
                    },
	  }
          });
        */

        $('.datepicker').datepicker({
                  autoclose: true,
                  format: "dd-mm-yyyy",
                  todayHighlight: true,
                  orientation: "top auto",
                  todayBtn: true,
                  todayHighlight: true,
              });



        $(document).ready(function(){
            $('.btn').animate({right: '150px'});
            $('.infolist').animate({left: '20px'});
            $('.form-group').animate({top: '20px'});


            $('#status').on('change', function (e) {

               val_instansi = this.value;

               var nilaidata = {
                status:val_instansi
               }

                   $.ajax({
                    type: "POST",
                    url: '<?php echo base_url($this->router->fetch_class()); ?>/ajax_filter_rekanan/',
                    data:nilaidata,
                    dataType: 'json',
                    success: function(data){
                      $("#rekanan").html(data.val);
                    },

                    error:function(XMLHttpRequest){
                      alert("Data Tidak Dapat Ditemukan");
                    }

                  })
                });
        });

        var id_upload;
        $(function() {
            $('.file_upload').uploadifive({
              'height'        : 30,
              'width'         : 120,
              'auto'             : true,
              'checkScript'      : '<?php echo base_url('project/uploadCheck') ?>',
              'onAddQueueItem' : function(file) {
                  this.data('uploadifive').settings.formData = {
              'someKey' : $(this).attr("name"),
              'timestamp' : '<?php echo $timestamp;?>',
              'token'     : '<?php echo $mytokenis?>',
              'keterangan' : '<?php echo $save_method ?>'
                };
              },
              'progressData' : 'speed',
              'multi'            : true,
              'method'           : 'post',
              'swf'      : '<?php echo base_url('assets'); ?>/plugins/uploadjquery/uploadify.swf',
              'queueID'          : 'queue',
              'uploadScript'     : '<?php echo base_url('project/upload') ?>',
              'onCancel'     : function(file) {
                // alert($('input[name="'+file.name+'"]').val());
                var id_lampiran = $('#temp_' + file.name.replace(/[^a-zA-Z0-9]/g,'_'));
                id_lampiran.remove();

                $.ajax({
                 type: "POST",
                 url: '<?php echo base_url('project/hapus_upload') ?>',
                 data:id_lampiran,
                 success: function(data){
                   console.log(data);
                 },

                 error:function(XMLHttpRequest){
                   alert("Data Tidak Dapat Ditemukan");
                 }

               })


              },
              'onUploadComplete' : function(file, data) {
                                    if (data=="outoflength") {
                                      alert("Nama file melebihi batas masuk");
                                    } else if(data=='infalidfiletype'){
                                      alert("File yang anda upload tidak valid");
                                    } else {
                                    $('#form').append('<input id="temp_' + file.name.replace(/[^a-zA-Z0-9]/g,'_') + '" class="form-control '+ file.name.replace(/[^a-zA-Z0-9]/g,'_') +'" type="hidden" name="file_lampiran[]" value="' + data +'" />');
                                    }
              }
            });
          });

          function cancel_before(id){
            var nama = $("#isi_file_edit_"+id).val();

            var dataini = {
              id:id,
              nama:nama,
              keterangan:"hapus_cancel"
            }

            $.ajax({
             type: "POST",
             url: '<?php echo base_url('project/hapus_upload') ?>',
             data:dataini,
             success: function(data){
               if (data=="successdel") {
                  $("#isi_file_edit_"+id).remove();
                  $("#file_edit_"+id).remove();
               }
             },

             error:function(XMLHttpRequest){
               alert("Data Tidak Dapat Ditemukan");
             }

           })
          }

        </script>
