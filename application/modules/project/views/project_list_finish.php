<div id="modal_resume" class="modal fade col-md-12" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header </h4>
      </div>
      <div class="modal-body">
      <div class="row text-center">
       <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body box-profile text-center">
              <table class="table table-striped table-bordered" id="isi_lampiran">
                <thead>
                  <tr>
                    <th>Nama File</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div> <!-- box-body -->
          </div>
        </div>


      </div> <!-- row -->
      <div class="modal-footer">
        <div id="editEmployeeButton"></div>
      </div>

      </div> <!-- modal-body -->
      </div> <!-- modal-content -->
    </div> <!-- modal-dialog -->
  </div> <!-- modal-resume -->



        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4 infolist">
                <h2 style="margin-top:0px">Project List</h2>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php echo anchor(site_url('project/create'), 'Tambah Data', 'class="btn btn-success hvr-float-shadow" style="position:relative" title="Tambah Data"'); ?>
		<?php echo anchor(site_url('project/excel'), 'Export Excel', 'class="btn btn-primary hvr-float-shadow" style="position:relative"'); ?>
	    </div>
        </div>
        <table class="table table-bordered table-striped" id="table">
            <thead>
                <tr>

		    <th style="min-width: 350px; ">Instansi</th>
		    <th >Nama Pekerjaan</th>
		    <th >Lokasi</th>
		    <th style="width:60px;" >Lampiran</th>
		    <th >Demo</th>
		    <th style="width:100px;">Action</th>
      </tr>
            </thead>
	    <tbody>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            var table;
            $(document).ready(function () {
              $('.btn').animate({right: '40px'});
              $('.breadcrumb').animate({top: '10px'});
              $('.infolist').animate({left: '40px'});
             table =  $("#table").DataTable({

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            responsive: true,
            "ajax": {
                "url": "<?php echo base_url($this->router->fetch_class()); ?>/ajax_list/finish",
                "type": "POST"
            },

            //Set column definition initialisation properties.
            "columnDefs": [
            {
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],

        });
            });


        function delete_action(id)
            {
                  if(confirm('Are you sure delete this data?'))
                  {
                      // ajax delete data to database
                      $.ajax({
                          url : "<?php echo base_url($this->router->fetch_class()); ?>/ajax_delete/"+id,
                          type: "POST",
                          dataType: "JSON",
                          success: function(data)
                          {
                              //if success reload ajax table
                              reload_table();
                          },

                          error: function (jqXHR, textStatus, errorThrown)
                          {
                              alert('Error deleting data');
                          }
                      });

                  }
              }

            function view_lampiran(id){

              $.ajax({
                url: "<?php echo base_url($this->router->fetch_class()); ?>/view_lampiran/"+id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data){
                    console.log(data)
                     $('#isi_lampiran').html(data.val);
                     $('#modal_resume').modal('show'); // show bootstrap modal when complete loaded
                     $('.modal-title').text('Lampiran'); // Set title to Bootstrap modal title

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error get data from ajax');
                }
              })

            }

            function hapus_lampiran(info){
              var id = info;
                alert(id);
            }

            function reload_table()
            {
                table.ajax.reload(null,false); //reload datatable ajax
            }

        </script>
