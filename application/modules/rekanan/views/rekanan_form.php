
        <div class="infolist" style="position:relative">
        <h2 style="margin-top:0px">Klien Pemerintah <?php echo $button ?></h2>
        </div>
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal" id="form">
        <div class="form-body">
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama Instansi </label>
            <div class="col-md-10">
            <select name="nama_instansi" id="nama_instansi" class="form-control" tabindex="1">
                <option value=" ">PILIH INSTANSI</option>
                <?php foreach ($list_instansi as $row): ?>
                    <option value="<?php echo $row->id_instansi; ?>" <?php if($nama_instansi==$row->id_instansi){ echo "selected"; } ?>><?php echo $row->nama_instansi; ?></option>
                <?php endforeach ?>
            </select>
            <div class="notif_error"><?php echo form_error('nama_instansi') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama Direktorat </label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="2" name="nama_direktorat" id="nama_direktorat" placeholder="Nama Direktorat" value="<?php echo $nama_direktorat; ?>" />
            <div class="notif_error"><?php echo form_error('nama_direktorat') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama PIC </label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="3" name="nama_pic" id="nama_pic" placeholder="Nama Pic" value="<?php echo $nama_pic; ?>" />
            <div class="notif_error"><?php echo form_error('nama_pic') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nomer Telfon PIC</label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="4" name="no_tlf_pic" id="no_tlf_pic" placeholder="No Tlf Pic" value="<?php echo $no_tlf_pic; ?>" />
            <div class="notif_error"><?php echo form_error('no_tlf_pic') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="alamat">Alamat </label>
            <div class="col-md-10">
            <textarea class="form-control" rows="3" name="alamat" id="alamat" tabindex="5" placeholder="Alamat"><?php echo $alamat; ?></textarea>
            <div class="notif_error"><?php echo form_error('alamat') ?></div>
            </div>
        </div>
	    <div class="form-group"> 
	    <div class="col-md-2"></div> 
	    <div class="col-md-3 control-label"> 
	    <input type="hidden" name="id_rekanan" style="position:relative" value="<?php echo $id_rekanan; ?>" /> 
	    <button type="submit" class="btn btn-success hvr-glow" style="position:relative" tabindex="5"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('rekanan') ?>" class="btn btn-warning hvr-glow" style="position:relative">Cancel</a>
	    </div> 
	    </div> 
	    </div> 
	    </form> 
	    
        <script type="text/javascript">
        /*
        jQuery.validator.setDefaults({
          debug: true,
          success: "valid"
          });
          var form = $( "#form" );
          form.validate({
          rules: { 
		   nama_instansi: { 
                    required:true 
                    },
		   nama_direktorat: { 
                    required:true 
                    },
		   nama_pic: { 
                    required:true 
                    },
		   no_tlf_pic: { 
                    required:true 
                    },
		   alamat: { 
                    required:true 
                    },
	  }
          });
        */
        $(document).ready(function(){
            $('.btn').animate({right: '150px'});
            $('.infolist').animate({left: '20px'});
            $('.form-group').animate({top: '20px'});
        });
        </script>  