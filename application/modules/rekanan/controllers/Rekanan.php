<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rekanan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        $this->load->model('Rekanan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->template->build('rekanan_list');
    }

    public function ajax_list()
    {
        $list = $this->Rekanan_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $nilaiku) {
            $no++;
            $row = array();
            $row[] = $no;
           $row[]= '<img src="'.base_url('uploads/foto').'/'.$nilaiku->logo_or.'" width="100px">';
		   $row[]= $nilaiku->nama_instansi;
		   $row[]= $nilaiku->nama_direktorat;
	        $row[] = '<a class="btn btn-sm btn-warning hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/read/'.$nilaiku->id_rekanan.'" title="Lihat Detail"><i class="glyphicon glyphicon-eye-open"></i></a>
                  <a class="btn btn-sm btn-info hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/update/'.$nilaiku->id_rekanan.'" title="Ubah Data"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger hvr-float-shadow"  href="javascript:void(0)" onclick="delete_action('."'".$nilaiku->id_rekanan."'".')" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>';


            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Rekanan_model->count_all(),
                        "recordsFiltered" => $this->Rekanan_model->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function read($id) 
    {
        $row = $this->Rekanan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_rekanan' => $row->id_rekanan,
		'nama_instansi' => $row->nama_instansi,
		'nama_direktorat' => $row->nama_direktorat,
		'nama_pic' => $row->nama_pic,
		'no_tlf_pic' => $row->no_tlf_pic,
		'alamat' => $row->alamat,
	    );
            $this->template->build('rekanan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('rekanan'));
        }
    }

    public function create() 
    {
        $data = array(
        'button' => 'Create',
        'action' => site_url('rekanan/create_action'),
	    'id_rekanan' => set_value('id_rekanan'),
	    'nama_instansi' => set_value('nama_instansi'),
	    'nama_direktorat' => set_value('nama_direktorat'),
	    'nama_pic' => set_value('nama_pic'),
	    'no_tlf_pic' => set_value('no_tlf_pic'),
        'alamat' => set_value('alamat'),
        'list_instansi' => $this->Rekanan_model->get_select_option("id_instansi,nama_instansi","instansi")
	   );


        $this->template->build('rekanan_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_instansi' => $this->input->post('nama_instansi',TRUE),
		'nama_direktorat' => $this->input->post('nama_direktorat',TRUE),
		'nama_pic' => $this->input->post('nama_pic',TRUE),
		'no_tlf_pic' => $this->input->post('no_tlf_pic',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Rekanan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('rekanan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Rekanan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('rekanan/update_action'),
		'id_rekanan' => set_value('id_rekanan', $row->id_rekanan),
		'nama_instansi' => set_value('nama_instansi', $row->nama_instansi),
		'nama_direktorat' => set_value('nama_direktorat', $row->nama_direktorat),
		'nama_pic' => set_value('nama_pic', $row->nama_pic),
		'no_tlf_pic' => set_value('no_tlf_pic', $row->no_tlf_pic),
		'alamat' => set_value('alamat', $row->alamat),
        'list_instansi' => $this->Rekanan_model->get_select_option("id_instansi,nama_instansi","instansi")
	    );
            $this->template->build('rekanan_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('rekanan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_rekanan', TRUE));
        } else {
            $data = array(
		'nama_instansi' => $this->input->post('nama_instansi',TRUE),
		'nama_direktorat' => $this->input->post('nama_direktorat',TRUE),
		'nama_pic' => $this->input->post('nama_pic',TRUE),
		'no_tlf_pic' => $this->input->post('no_tlf_pic',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Rekanan_model->update($this->input->post('id_rekanan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('rekanan'));
        }
    }


    public function ajax_delete($id)
    {
        
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($this->Rekanan_model->delete_by_id($id,'id_rekanan')));
    }
    
    public function delete($id) 
    {
        $row = $this->Rekanan_model->get_by_id($id);

        if ($row) {
            $this->Rekanan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('rekanan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('rekanan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_direktorat', 'nama direktorat', 'trim|required');

	$this->form_validation->set_rules('id_rekanan', 'id_rekanan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

