<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
		
	}

	public function index()
	{
		$this->template->prepend_metadata('
				<!-- Sparkline -->
			    <script src="'.base_url('assets').'/plugins/sparkline/jquery.sparkline.min.js"></script>
			    <!-- jvectormap -->
			    <script src="'.base_url('assets').'/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
			    <script src="'.base_url('assets').'/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
			    <!-- ChartJS 1.0.1 -->
			    <script src="'.base_url('assets').'/plugins/chartjs/Chart.min.js"></script>
			    <!-- custom -->
			    <script src="'.base_url('assets').'/custom/dashboard_custom.js"></script>
			');

		$this->template->build('index');	
	}

}

/* End of file Dashboard.php */
/* Location: ./application/modules/Dashboard/controllers/Dashboard.php */