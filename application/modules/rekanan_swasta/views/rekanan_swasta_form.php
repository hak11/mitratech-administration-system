
        <div class="infolist" style="position:relative">
        <h2 style="margin-top:0px">Klien Swasta <?php echo $button ?></h2>
        </div>
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal" id="form">
        <div class="form-body">
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama Rekanan </label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="1" name="nama_rekanan" id="nama_rekanan" placeholder="Nama Rekanan" value="<?php echo $nama_rekanan; ?>" />
            <div class="notif_error"><?php echo form_error('nama_rekanan') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">No Tlf </label>
            <div class="col-md-10">
            <input type="number" class="form-control" tabindex="2" name="no_tlf" id="no_tlf" placeholder="No Tlf" value="<?php echo $no_tlf; ?>" />
            <div class="notif_error"><?php echo form_error('no_tlf') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama Pic </label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="3" name="nama_pic" id="nama_pic" placeholder="Nama Pic" value="<?php echo $nama_pic; ?>" />
            <div class="notif_error"><?php echo form_error('nama_pic') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">No Tlf Pic </label>
            <div class="col-md-10">
            <input type="number" class="form-control" tabindex="4" name="no_tlf_pic" id="no_tlf_pic" placeholder="No Tlf Pic" value="<?php echo $no_tlf_pic; ?>" />
            <div class="notif_error"><?php echo form_error('no_tlf_pic') ?></div>
            </div>
        </div>
	    
	    <div class="form-group">
            <label class="control-label col-md-2" for="alamat">Alamat </label>
            <div class="col-md-10">
            <textarea class="form-control" rows="3" name="alamat" id="alamat" tabindex="6" placeholder="Alamat"><?php echo $alamat; ?></textarea>
            <div class="notif_error"><?php echo form_error('alamat') ?></div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="logo">Logo </label>
            <div class="col-md-10 uploadimg">
            <input id="file_upload" type="file" name="logo"  multiple="false" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="varchar"></label>
            <div class="col-md-4">
            <?php if ($method_input!="add") {
              if ($logo!='noimage.jpg'){ ?>
              <img src="<?php echo base_url('uploads/logo_swasta').'/'.$logo ?>" id="user_photo" width="200px">
              <?php } else { ?>
              <img src="<?php echo base_url('uploads/logo_swasta').'/noimage.jpg' ?>" id="user_photo" width="200px">  
              <?php } 
            } else { ?>
              <img src="<?php echo base_url('uploads/logo_swasta').'/noimage.jpg' ?>" id="user_photo" width="200px">
            <?php } ?>

            <button type="button" class="btn btn-danger cancelimg" onclick="removeimg()"><i class='fa fa-trash'></i></button>
            <div class="notif_error"><div id="queue"></div></div>

            </div>
         </div>
	    <div class="form-group"> 
	    <div class="col-md-2"></div> 
	    <div class="col-md-3 control-label"> 
	    <input type="hidden" name="id_rekanan_swasta" style="position:relative" value="<?php echo $id_rekanan_swasta; ?>" />
        <input type="hidden" name="logo" style="position:relative" value="<?php echo $logo; ?>" id="logo" /> 
	    <button type="submit" class="btn btn-success hvr-glow" style="position:relative" tabindex="6"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('rekanan_swasta') ?>" class="btn btn-warning hvr-glow" style="position:relative">Cancel</a>
	    </div> 
	    </div> 
	    </div> 
	    </form> 
	    
        <script type="text/javascript">
        /*
        jQuery.validator.setDefaults({
          debug: true,
          success: "valid"
          });
          var form = $( "#form" );
          form.validate({
          rules: { 
		   nama_rekanan: { 
                    required:true 
                    },
		   no_tlf: { 
                    required:true 
                    },
		   nama_pic: { 
                    required:true 
                    },
		   no_tlf_pic: { 
                    required:true 
                    },
		   logo: { 
                    required:true 
                    },
		   alamat: { 
                    required:true 
                    },
	  }
          });
        */
       
        var method_input;
       var logoku;

       method_input = "<?php echo $method_input ?>";
       logoku = "<?php echo $logo ?>";

        $(document).ready(function(){
            $('.btn').animate({right: '150px'});
            $('.infolist').animate({left: '20px'});
            $('.form-group').animate({top: '20px'});
            if (method_input=="add" || logoku=="noimage.jpg") {
            $('.cancelimg').hide();                            
            } else if(method_input=="edit"){
            $(".uploadimg").hide('slow');
            }

        });
        <?php $timestamp = time();?>

        $(function() {
            $('#file_upload').uploadifive({
              'auto'             : true,
              'checkScript'      : '<?php echo base_url('rekanan_swasta/uploadCheck') ?>',
              'formData'         : {
                           'timestamp' : '<?php echo $timestamp;?>',
                           'token'     : '<?php echo md5('unique_salt' . $timestamp);?>'

                                   },
              'progressData' : 'speed',
              'multi'            : false,
              'method'           : 'post',
              'swf'      : '<?php echo base_url('assets'); ?>/plugins/uploadjquery/uploadify.swf',
              'queueID'          : 'queue',
              'uploadScript'     : '<?php echo base_url('rekanan_swasta/upload') ?>',
              'onUploadComplete' : function(file, data) { 

                                  $("#user_photo").attr("src", "<?php echo base_url(); ?>/uploads/logo_swasta/"+data);
                                  $("#logo").attr("value", data);
                                  $(".uploadimg").hide('slow');
                                  $(".cancelimg").show('slow');

                                    console.log(file);
                                    console.log(data);
                                  }
            });
          });

        function removeimg(){
          $("#user_photo").attr("src", "<?php echo base_url(); ?>/uploads/logo_swasta/noimage.jpg");
          $("#logo").attr("value", "noimage.jpg");
          $(".uploadimg").show('slow');
          $(".cancelimg").hide('slow');
          $(".complete").hide('slow');
        }
        </script>  