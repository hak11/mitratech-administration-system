<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rekanan_swasta extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        $this->load->model('Rekanan_swasta_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->template->build('rekanan_swasta_list');
    }

    public function ajax_list()
    {
        $list = $this->Rekanan_swasta_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $nilaiku) {
            $no++;
            $row = array();

           $row[]= '<img src="'.base_url('uploads/logo_swasta').'/'.$nilaiku->logo.'" width="100px">';
		   $row[]= $nilaiku->nama_rekanan;
		   $row[]= $nilaiku->alamat;
	        $row[] = '<a class="btn btn-sm btn-warning hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/read/'.$nilaiku->id_rekanan_swasta.'" title="Lihat Detail"><i class="glyphicon glyphicon-eye-open"></i></a>
                  <a class="btn btn-sm btn-info hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/update/'.$nilaiku->id_rekanan_swasta.'" title="Ubah Data"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger hvr-float-shadow"  href="javascript:void(0)" onclick="delete_action('."'".$nilaiku->id_rekanan_swasta."'".')" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>';


            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Rekanan_swasta_model->count_all(),
                        "recordsFiltered" => $this->Rekanan_swasta_model->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function read($id) 
    {
        $row = $this->Rekanan_swasta_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_rekanan_swasta' => $row->id_rekanan_swasta,
		'nama_rekanan' => $row->nama_rekanan,
		'no_tlf' => $row->no_tlf,
		'nama_pic' => $row->nama_pic,
		'no_tlf_pic' => $row->no_tlf_pic,
		'logo' => $row->logo,
		'alamat' => $row->alamat,
	    );
            $this->template->build('rekanan_swasta_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('rekanan_swasta'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('rekanan_swasta/create_action'),
	    'id_rekanan_swasta' => set_value('id_rekanan_swasta'),
	    'nama_rekanan' => set_value('nama_rekanan'),
	    'no_tlf' => set_value('no_tlf'),
	    'nama_pic' => set_value('nama_pic'),
	    'no_tlf_pic' => set_value('no_tlf_pic'),
	    'logo' => set_value('logo'),
	    'alamat' => set_value('alamat'),
        'method_input' => "add",

	);
        $this->template->build('rekanan_swasta_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->input->post('logo',TRUE)) {
            $logo = $this->input->post('logo',TRUE);
        } else {
            $logo = "noimage.jpg";
        }

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_rekanan' => $this->input->post('nama_rekanan',TRUE),
		'no_tlf' => $this->input->post('no_tlf',TRUE),
		'nama_pic' => $this->input->post('nama_pic',TRUE),
        'no_tlf_pic' => $this->input->post('no_tlf_pic',TRUE),
		'logo' => $logo,
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Rekanan_swasta_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('rekanan_swasta'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Rekanan_swasta_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('rekanan_swasta/update_action'),
		'id_rekanan_swasta' => set_value('id_rekanan_swasta', $row->id_rekanan_swasta),
		'nama_rekanan' => set_value('nama_rekanan', $row->nama_rekanan),
		'no_tlf' => set_value('no_tlf', $row->no_tlf),
		'nama_pic' => set_value('nama_pic', $row->nama_pic),
		'no_tlf_pic' => set_value('no_tlf_pic', $row->no_tlf_pic),
		'logo' => set_value('logo', $row->logo),
		'alamat' => set_value('alamat', $row->alamat),
        'method_input' => "edit",
	    );
            $this->template->build('rekanan_swasta_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('rekanan_swasta'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_rekanan_swasta', TRUE));
        } else {
            $data = array(
		'nama_rekanan' => $this->input->post('nama_rekanan',TRUE),
		'no_tlf' => $this->input->post('no_tlf',TRUE),
		'nama_pic' => $this->input->post('nama_pic',TRUE),
		'no_tlf_pic' => $this->input->post('no_tlf_pic',TRUE),
		'logo' => $this->input->post('logo',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Rekanan_swasta_model->update($this->input->post('id_rekanan_swasta', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('rekanan_swasta'));
        }
    }


    public function ajax_delete($id)
    {
        
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($this->Rekanan_swasta_model->delete_by_id($id,'id_rekanan_swasta')));
    }
    
    public function delete($id) 
    {
        $row = $this->Rekanan_swasta_model->get_by_id($id);

        if ($row) {
            $this->Rekanan_swasta_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('rekanan_swasta'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('rekanan_swasta'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_rekanan', 'nama rekanan', 'trim|required');

	$this->form_validation->set_rules('id_rekanan_swasta', 'id_rekanan_swasta', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


    public function uploadCheck()
    {
        $baseproject ="/mitratechapp";
        $targetFolder = '/uploads/logo_swasta'; // Relative to the root and should match the upload folder in the uploader script

        if (file_exists($isi = $_SERVER['DOCUMENT_ROOT'].$baseproject.$targetFolder . '/' . $_POST['filename'])) {
            echo 1;
        } else {
            echo 0;
        }
    }


    public function upload()
    {
       
        $targetFolder = './uploads/logo_swasta'; // Relative to the root

        $verifyToken = md5('unique_salt' . $_POST['timestamp']);

        if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = $targetFolder;
            // $targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];
            $new_filename = $verifyToken.$_FILES['Filedata']['name'];
            $targetFile = rtrim($targetPath, '/') . '/' . $new_filename;
            $nama_file = $_FILES['Filedata']['name'];
            
            // Validate the file type
            $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
            $fileParts = pathinfo($_FILES['Filedata']['name']);
            
            if (in_array($fileParts['extension'],$fileTypes)) {
                move_uploaded_file($tempFile,$targetFile);
                $array = array(
                    'foto' => $new_filename,
                );
                
                $this->session->set_flashdata( $array );
                echo $new_filename;
            } else {
                echo 'Invalid file type.';
            }
        }
    }




}

