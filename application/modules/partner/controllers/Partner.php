<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Partner extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        $this->load->model('Partner_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->template->build('partner_list');
    }

    public function ajax_list()
    {
        $list = $this->Partner_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $nilaiku) {
            $no++;
            $row = array();
            $row[] = $no;

           $row[]= $nilaiku->nama;
           $row[]= $nilaiku->nama_pic;
           $row[]= $nilaiku->no_tlf;
           $row[]= $nilaiku->email;
           $row[]= $nilaiku->alamat;
            $row[] = '<a class="btn btn-sm btn-warning hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/read/'.$nilaiku->id_partner.'" title="Lihat Detail"><i class="glyphicon glyphicon-eye-open"></i></a>
                  <a class="btn btn-sm btn-info hvr-float-shadow" href="'.base_url($this->router->fetch_class()).'/update/'.$nilaiku->id_partner.'" title="Ubah Data"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger hvr-float-shadow"  href="javascript:void(0)" onclick="delete_action('."'".$nilaiku->id_partner."'".')" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>';


            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Partner_model->count_all(),
                        "recordsFiltered" => $this->Partner_model->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function read($id) 
    {
        $row = $this->Partner_model->get_by_id($id);
        if ($row) {
            $data = array(
        'id_partner' => $row->id_partner,
        'nama' => $row->nama,
        'nama_pic' => $row->nama_pic,
        'no_tlf' => $row->no_tlf,
        'email' => $row->email,
        'alamat' => $row->alamat,
        );
            $this->template->build('partner_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('partner'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('partner/create_action'),
        'id_partner' => set_value('id_partner'),
        'nama' => set_value('nama'),
        'nama_pic' => set_value('nama_pic'),
        'no_tlf' => set_value('no_tlf'),
        'email' => set_value('email'),
        'alamat' => set_value('alamat'),
    );
        $this->template->build('partner_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
        'nama' => $this->input->post('nama',TRUE),
        'nama_pic' => $this->input->post('nama_pic',TRUE),
        'no_tlf' => $this->input->post('no_tlf',TRUE),
        'email' => $this->input->post('email',TRUE),
        'alamat' => $this->input->post('alamat',TRUE),
        );

            $this->Partner_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('partner'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Partner_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('partner/update_action'),
        'id_partner' => set_value('id_partner', $row->id_partner),
        'nama' => set_value('nama', $row->nama),
        'nama_pic' => set_value('nama_pic', $row->nama_pic),
        'no_tlf' => set_value('no_tlf', $row->no_tlf),
        'email' => set_value('email', $row->email),
        'alamat' => set_value('alamat', $row->alamat),
        );
            $this->template->build('partner_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('partner'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_partner', TRUE));
        } else {
            $data = array(
        'nama' => $this->input->post('nama',TRUE),
        'nama_pic' => $this->input->post('nama_pic',TRUE),
        'no_tlf' => $this->input->post('no_tlf',TRUE),
        'email' => $this->input->post('email',TRUE),
        'alamat' => $this->input->post('alamat',TRUE),
        );

            $this->Partner_model->update($this->input->post('id_partner', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('partner'));
        }
    }


    public function ajax_delete($id)
    {
        
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($this->Partner_model->delete_by_id($id,'id_partner')));
    }
    
    public function delete($id) 
    {
        $row = $this->Partner_model->get_by_id($id);

        if ($row) {
            $this->Partner_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('partner'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('partner'));
        }
    }

    public function _rules() 
    {
    $this->form_validation->set_rules('nama', 'nama', 'trim|required');

    $this->form_validation->set_rules('id_partner', 'id_partner', 'trim');
    $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "partner.xls";
        $judul = "partner";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
    xlsWriteLabel($tablehead, $kolomhead++, "Nama");
    xlsWriteLabel($tablehead, $kolomhead++, "Nama Pic");
    xlsWriteLabel($tablehead, $kolomhead++, "No Tlf");
    xlsWriteLabel($tablehead, $kolomhead++, "Email");
    xlsWriteLabel($tablehead, $kolomhead++, "Alamat");

    foreach ($this->Partner_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
        xlsWriteLabel($tablebody, $kolombody++, $data->nama);
        xlsWriteLabel($tablebody, $kolombody++, $data->nama_pic);
        xlsWriteLabel($tablebody, $kolombody++, $data->no_tlf);
        xlsWriteLabel($tablebody, $kolombody++, $data->email);
        xlsWriteLabel($tablebody, $kolombody++, $data->alamat);

        $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

