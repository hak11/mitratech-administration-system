
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4 infolist">
                <h2 style="margin-top:0px">Partner List</h2>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php echo anchor(site_url('partner/create'), 'Tambah Data', 'class="btn btn-success hvr-float-shadow" style="position:relative" title="Tambah Data"'); ?>
    <?php echo anchor(site_url('partner/excel'), 'Export Excel', 'class="btn btn-primary hvr-float-shadow" style="position:relative"'); ?>
      </div>
        </div>
        <table class="table table-bordered table-striped" id="table">
            <thead>
                <tr>
                    <th width="80px">No</th>
        <th>Nama</th>
        <th>Nama Pic</th>
        <th>No Tlf</th>
        <th>Email</th>
        <th>Alamat</th>
        <th>Action</th>
                </tr>
            </thead>
      <tbody>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            var table;
            $(document).ready(function () {
              $('.btn').animate({right: '40px'});
              $('.breadcrumb').animate({top: '10px'});
              $('.infolist').animate({left: '40px'});
             table =  $("#table").DataTable({ 

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            "ajax": {
                "url": "<?php echo base_url($this->router->fetch_class()); ?>/ajax_list",
                "type": "POST"
            },

            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],

        });
            });


        function delete_action(id)
            {
                  if(confirm('Are you sure delete this data?'))
                  {
                      // ajax delete data to database
                      $.ajax({
                          url : "<?php echo base_url($this->router->fetch_class()); ?>/ajax_delete/"+id,
                          type: "POST",
                          dataType: "JSON",
                          success: function(data)
                          {
                              //if success reload ajax table
                              reload_table();
                          },
                          
                          error: function (jqXHR, textStatus, errorThrown)
                          {
                              alert('Error deleting data');
                          }
                      });

                  }
              }
              
            function reload_table()
            {
                table.ajax.reload(null,false); //reload datatable ajax 
            }
        </script>
    