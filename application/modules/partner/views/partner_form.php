
        <div class="infolist" style="position:relative">
        <h2 style="margin-top:0px">Partner <?php echo $button ?></h2>
        </div>
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal" id="form">
        <div class="form-body">
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama </label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="1" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
            <div class="notif_error"><?php echo form_error('nama') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama Pic </label>
            <div class="col-md-10">
            <input type="text" class="form-control" tabindex="2" name="nama_pic" id="nama_pic" placeholder="Nama Pic" value="<?php echo $nama_pic; ?>" />
            <div class="notif_error"><?php echo form_error('nama_pic') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">No Tlf </label>
            <div class="col-md-10">
            <input type="number" class="form-control" tabindex="3" name="no_tlf" id="no_tlf" placeholder="No Tlf" value="<?php echo $no_tlf; ?>" />
            <div class="notif_error"><?php echo form_error('no_tlf') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Email </label>
            <div class="col-md-10">
            <input type="email" class="form-control" tabindex="4" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
            <div class="notif_error"><?php echo form_error('email') ?></div>
            </div>
        </div>
	    <div class="form-group">
            <label class="control-label col-md-2" for="alamat">Alamat </label>
            <div class="col-md-10">
            <textarea class="form-control" rows="3" name="alamat" id="alamat" tabindex="5" placeholder="Alamat"><?php echo $alamat; ?></textarea>
            <div class="notif_error"><?php echo form_error('alamat') ?></div>
            </div>
        </div>
	    <div class="form-group"> 
	    <div class="col-md-2"></div> 
	    <div class="col-md-3 control-label"> 
	    <input type="hidden" name="id_partner" style="position:relative" value="<?php echo $id_partner; ?>" /> 
	    <button type="submit" class="btn btn-success hvr-glow" style="position:relative" tabindex="5"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('partner') ?>" class="btn btn-warning hvr-glow" style="position:relative">Cancel</a>
	    </div> 
	    </div> 
	    </div> 
	    </form> 
	    
        <script type="text/javascript">
        /*
        jQuery.validator.setDefaults({
          debug: true,
          success: "valid"
          });
          var form = $( "#form" );
          form.validate({
          rules: { 
		   nama: { 
                    required:true 
                    },
		   nama_pic: { 
                    required:true 
                    },
		   no_tlf: { 
                    required:true 
                    },
		   email: { 
                    required:true 
                    },
		   alamat: { 
                    required:true 
                    },
	  }
          });
        */
        $(document).ready(function(){
            $('.btn').animate({right: '150px'});
            $('.infolist').animate({left: '20px'});
            $('.form-group').animate({top: '20px'});
        });
        </script>  