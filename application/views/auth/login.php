<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>WEB PORTAL | PT.MITRATECH ANDAL SINERGIA</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/custom/custom.css">
  </head>
  <body class="hold-transition">
    <nav class="navbar navbar-inverse navbar-static-top">
    </nav>

    <div class="container">
        <div class="row">
          <div class="col-md-6 col-xs-12 text-center">

            <img src="<?php echo base_url('themes'); ?>/img/logo_mitratech.png" class="img-responsive"></a>
          </div>


           <div class="col-md-6 col-xs-12">
            <div class="login-box">
            <div class="login-logo">
              <a href="#">Silakan Login</a>
            </div><!-- /.login-logo -->

            <div class="login-box-body custom-login">
              <?php echo form_open("auth/login");?>
                <div class="form-group has-feedback">
                  <input type="text" name="identity" id="identity" class="form-control">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                  <input type="password" name="password" id="password" class="form-control">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                  <div class="col-xs-8">
                  </div><!-- /.col -->
                  <div class="col-xs-4">
                  <p><?php echo form_submit('submit', lang('login_submit_btn'),'class="form-control"');?></p>                  </div><!-- /.col -->
                </div>
              <?php echo form_close();?>
            </div><!-- /.login-box-body -->
          </div><!-- /.login-box -->
          </div><!-- col-md-6-->
        </div>  <!-- row -->
    </div><!-- container -->

    <footer class="footer">
      <div class="footer-above">
      </div>
      <div class="footer-bellow">
        <div class="container">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2016 <a href="#">Abdul Jabar Hakim</a>.</strong> Mitratech Management System.
      </div>
        </div>
    </footer>

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('assets'); ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url('assets'); ?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url('assets'); ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('assets'); ?>/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('assets'); ?>/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('assets'); ?>/dist/js/demo.js"></script>

    <script src="<?php echo base_url('assets'); ?>/custom/custom.js"></script>
  </body>
</html>
