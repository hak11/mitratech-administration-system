        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url('assets'); ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $this->ion_auth->user()->row()->first_name ?> <?php echo $this->ion_auth->user()->row()->last_name ?></p>
              <a href="#"></i> <?php echo $this->ion_auth->user()->row()->email ?></a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENU NAVIGASI</li>
            <li class="treeview <?php if ( $this->uri->uri_string() == 'dashboard' ){ echo "active"; } ?>">
              <a href="<?php echo base_url('dashboard'); ?>" class="hvr-sweep-to-right">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
            <li class="<?php if ( $this->uri->uri_string() == 'project' OR $this->uri->uri_string() == 'project/create' OR $this->uri->uri_string() == 'project/finish'){ echo "active"; } ?>">
              <a href="#" class="hvr-icon-hang">
                <i class="fa fa-edit"></i> <span>PROJECT</span>
              </a>
              <ul class="treeview-menu">
                <li class="<?php if ( $this->uri->uri_string() == 'project/create' ){ echo "active"; } ?>"><a href="<?php echo base_url('project/create'); ?>" class="hvr-sweep-to-right"><i class="fa fa-circle-o text-yellow" class="hvr-sweep-to-right"></i> TAMBAH PROJECT</a></li>
                <li class="<?php if ( $this->uri->uri_string() == 'project' ){ echo "active"; } ?>"><a href="<?php echo base_url('project'); ?>" class="hvr-sweep-to-right"><i class="fa fa-circle-o text-yellow" class="hvr-sweep-to-right"></i> ON GOING</a></li>
                <li class="<?php if ( $this->uri->uri_string() == 'project/finish' ){ echo "active"; } ?>"><a href="<?php echo base_url('project/finish'); ?>" class="hvr-sweep-to-right"><i class="fa fa-circle-o text-yellow" class="hvr-sweep-to-right"></i> FINISHED</a></li>
              </ul>
            </li>
            <li class="treeview <?php if ( $this->uri->uri_string() == 'tenaga_ahli' ){ echo "active"; } ?>">
              <a href="<?php echo base_url('tenaga_ahli'); ?>" class="hvr-sweep-to-right">
                <i class="fa fa-dashboard"></i> <span>TENAGA AHLI</span></i>
              </a>
            </li>
            <li  class="<?php if ( $this->uri->uri_string() == 'partner' OR $this->uri->uri_string() =='rekanan' OR $this->uri->uri_string() =='instansi' OR $this->uri->uri_string() =='rekanan_swasta'){ echo "active"; } ?>">
              <a href="#" class="hvr-icon-hang">
                <i class="fa fa-cogs"></i> <span>KLIEN</span>
              </a>
              <ul class="treeview-menu">
                <li class="<?php if ( $this->uri->uri_string() == 'instansi' ){ echo "active"; } ?>"><a href="<?php echo base_url('instansi'); ?>" class="hvr-sweep-to-right"><i class="fa fa-circle-o text-red"></i> KLIEN PEMERINTAH</a></li>
                <li class="<?php if ( $this->uri->uri_string() == 'rekanan' ){ echo "active"; } ?>"><a href="<?php echo base_url('rekanan'); ?>" class="hvr-sweep-to-right"><i class="fa fa-circle-o text-red"></i> KLIEN PEMERINTAH</a></li>
                <li class="<?php if ( $this->uri->uri_string() == 'rekanan_swasta' ){ echo "active"; } ?>"><a href="<?php echo base_url('rekanan_swasta'); ?>" class="hvr-sweep-to-right"><i class="fa fa-circle-o text-red"></i> KLIEN SWASTA</a></li>
              </ul>
            </li>
            <li>
              <a href="#" class="hvr-icon-hang">
                <i class="fa fa-user"></i> <span>MANAGEMENT USER</span>
              </a>
              <ul class="treeview-menu">
                <li class="<?php if ( $this->uri->uri_string() == 'management_user' ){ echo "active"; } ?>"><a href="<?php echo base_url('management_user'); ?>" class="hvr-sweep-to-right"><i class="fa fa-circle-o text-yellow"></i> TAMBAH USER</a></li>
                <li class="<?php if ( $this->uri->uri_string() == 'kelola_hak_akses' ){ echo "active"; } ?>"><a href="<?php echo base_url('kelola_hak_akses'); ?>" class="hvr-sweep-to-right"><i class="fa fa-circle-o text-yellow"></i> KELOLA HAK AKSES</a></li>
              </ul>
            </li>
          </ul>
        </section>
